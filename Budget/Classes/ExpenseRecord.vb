﻿' Project Name:  Budget
' Copyright 2018 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 23 June 2018

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class ExpenseRecord
    Private _ExpenseId As Integer

    ''' <summary>
    ''' The unique Primary Key for each expense record.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseId() As Integer
        Get
            Return _ExpenseId
        End Get
        Set(ByVal value As Integer)
            _ExpenseId = value
        End Set
    End Property

    Private _ExpenseDescription As String

    ''' <summary>
    ''' A short description that indicates what this expense is for.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseDescription() As String
        Get
            Return _ExpenseDescription
        End Get
        Set(ByVal value As String)
            _ExpenseDescription = value
        End Set
    End Property

    Private _ExpenseAmount As Decimal

    ''' <summary>
    ''' The estimated amount that will be required to pay this expense.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseAmount() As Decimal
        Get
            Return _ExpenseAmount
        End Get
        Set(ByVal value As Decimal)
            _ExpenseAmount = value
        End Set
    End Property

    Private _ExpenseRecurranceTypeCode As String

    ''' <summary>
    ''' Indicates the frequency that this expense occurs (DAILY, WEEKLY, MONTHLY, LASTDAYOFMONTH, YEARLY).
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseRecurranceTypeCode() As String
        Get
            Return _ExpenseRecurranceTypeCode
        End Get
        Set(ByVal value As String)
            _ExpenseRecurranceTypeCode = value
        End Set
    End Property

    Private _ExpenseRecurranceInterval As Integer

    ''' <summary>
    ''' Indicates how often this expense repeats within its recurrence type (once a week, twice a week, etc).  Used in conjunction with ExpenseRecurranceTypeCode.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseRecurranceInterval() As Integer
        Get
            Return _ExpenseRecurranceInterval
        End Get
        Set(ByVal value As Integer)
            _ExpenseRecurranceInterval = value
        End Set
    End Property

    Private _ExpenseNextRecurranceTypeCode As String

    ''' <summary>
    ''' Indicates how the next due date should be calculated.  Either based on the inital due date (always 1st of the month) or based on the last time this expense was paid.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseNextRecurranceTypeCode() As String
        Get
            Return _ExpenseNextRecurranceTypeCode
        End Get
        Set(ByVal value As String)
            _ExpenseNextRecurranceTypeCode = value
        End Set
    End Property

    Private _ExpenseStartDate As Date

    ''' <summary>
    ''' Indicates the first due date for this expense.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseStartDate() As Date
        Get
            Return _ExpenseStartDate
        End Get
        Set(ByVal value As DateTime)
            _ExpenseStartDate = value
        End Set
    End Property

    Private _ExpenseEndDate As Date?

    ''' <summary>
    ''' Indicates the last due date for this expense.  Can be blank if this expense has no end date.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseEndDate() As Date?
        Get
            Return _ExpenseEndDate
        End Get
        Set(ByVal value As Date?)
            _ExpenseEndDate = value
        End Set
    End Property

    Private _ExpenseLastPaidDate As Date?

    ''' <summary>
    ''' Indicates the last time that this expense was paid.  Can be blank if this is a new expense that has never been paid.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseLastPaidDate() As Date?
        Get
            Return _ExpenseLastPaidDate
        End Get
        Set(ByVal value As Date?)
            _ExpenseLastPaidDate = value
        End Set
    End Property

    Private _ExpenseNextDueDate As Date?

    ''' <summary>
    ''' Indicates the next due date for this expense.  Can be blank if End Date has past.  Used for display purposes only to show the next estimated due date.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseNextDueDate() As Date?
        Get
            Return _ExpenseNextDueDate
        End Get
        Set(ByVal value As Date?)
            _ExpenseNextDueDate = value
        End Set
    End Property

    Private _ExpenseType As String

    ''' <summary>
    ''' Indicates if this record is for an income or expense item.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseType() As String
        Get
            Return _ExpenseType
        End Get
        Set(ByVal value As String)
            _ExpenseType = value
        End Set
    End Property
End Class
