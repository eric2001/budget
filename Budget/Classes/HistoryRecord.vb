﻿' Project Name:  Budget
' Copyright 2018 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 23 June 2018

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class HistoryRecord
    Private _ExpenseHistoryId As Integer

    ''' <summary>
    ''' The Unique Primary Key for each history record.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseHistoryId() As Integer
        Get
            Return _ExpenseHistoryId
        End Get
        Set(ByVal value As Integer)
            _ExpenseHistoryId = value
        End Set
    End Property

    Private _ExpenseId As Integer

    ''' <summary>
    ''' The foreign key to the corresponding Expense record.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseId() As Integer
        Get
            Return _ExpenseId
        End Get
        Set(ByVal value As Integer)
            _ExpenseId = value
        End Set
    End Property

    Private _ExpenseEstimatedDate As Date

    ''' <summary>
    ''' The date that this expense was estimated to be due / paid on.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseEstimatedDate() As Date
        Get
            Return _ExpenseEstimatedDate
        End Get
        Set(ByVal value As Date)
            _ExpenseEstimatedDate = value
        End Set
    End Property

    Private _ExpenseActualDate As Date?

    ''' <summary>
    ''' The Date that this expense was actually paid on.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseActualDate() As Date?
        Get
            Return _ExpenseActualDate
        End Get
        Set(ByVal value As Date?)
            _ExpenseActualDate = value
        End Set
    End Property

    Private _ExpenseEstimatedAmount As Decimal

    ''' <summary>
    ''' The estimated amount that is expected to be paid for this expense.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpenseEstimatedAmount() As Decimal
        Get
            Return _ExpenseEstimatedAmount
        End Get
        Set(ByVal value As Decimal)
            _ExpenseEstimatedAmount = value
        End Set
    End Property

    Private _ExpensePaidAmount As Decimal

    ''' <summary>
    ''' The Amount that was paid.  Tracked in case it's different from the estimated amount.
    ''' </summary>
    ''' <returns></returns>
    Public Property ExpensePaidAmount() As Decimal
        Get
            Return _ExpensePaidAmount
        End Get
        Set(ByVal value As Decimal)
            _ExpensePaidAmount = value
        End Set
    End Property

    Private _ExpenseMemo As String

    ''' <summary>
    ''' A short note or description about this specific payment.
    ''' </summary>
    Public Property ExpenseMemo() As String
        Get
            Return _ExpenseMemo
        End Get
        Set(ByVal value As String)
            _ExpenseMemo = value
        End Set
    End Property
End Class
