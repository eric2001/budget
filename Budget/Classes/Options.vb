﻿' Project Name:  Budget
' Copyright 2018 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 23 June 2018

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Imports System.ComponentModel
Imports System.Reflection

Public Class Options

#Region "Enums"

    ''' <summary>
    ''' Contains the names of the tables in the data source.
    ''' </summary>
    Public Enum Tables

        ''' <summary>
        ''' Contains all of the available recurring income and expense configurations.
        ''' </summary>
        Expense

        ''' <summary>
        ''' Contains the history of all of the paid / received expenses and incomes.
        ''' </summary>
        ExpenseHistory

        ''' <summary>
        ''' Contains any notes that the user has saved.
        ''' </summary>
        Notes
    End Enum

    ''' <summary>
    ''' Indicates if the record is an income or expense record.
    ''' </summary>
    Public Enum ExpenseType

        ''' <summary>
        ''' Indicates that this record is for a recurring income record.
        ''' </summary>
        <DescriptionAttribute("Recurring Income")>
        INCOME

        ''' <summary>
        ''' Indicates that this record is for a recurring expense record.
        ''' </summary>
        <DescriptionAttribute("Recurring Expense")>
        EXPENSE

    End Enum

    ''' <summary>
    ''' Contains a list of available recurrances for each expense.
    ''' </summary>
    Public Enum RecurranceType

        ''' <summary>
        ''' This Expense recurs Daily, or every X Days.
        ''' </summary>
        <DescriptionAttribute("Daily Expenses")>
        DAILY

        ''' <summary>
        ''' This Expense recurs Weekly, or every X Weeks.
        ''' </summary>
        <DescriptionAttribute("Weekly Expenses")>
        WEEKLY

        ''' <summary>
        ''' This Expense recurs Monthly, or every X Months.
        ''' </summary>
        <DescriptionAttribute("Monthly Expenses")>
        MONTHLY

        ''' <summary>
        ''' This Expense recurs at the last day of each month.
        ''' </summary>
        <DescriptionAttribute("Last Day Of The Month")>
        LASTDAYOFMONTH

        ''' <summary>
        ''' This Expense recurs Yearly, or every X Years.
        ''' </summary>
        <DescriptionAttribute("Yearly Expenses")>
        YEARLY

    End Enum

    ''' <summary>
    ''' Contains a list of available calculation types for each expense.
    ''' </summary>
    Public Enum CalculationType

        ''' <summary>
        ''' This Expense repeats based on the initial due date.  
        ''' For example, if it's a monthly expense that's due on the first of the month, then it's always due on the first of the month.
        ''' </summary>
        <DescriptionAttribute("Initial Due Date")>
        STARTDATE

        ''' <summary>
        ''' This Expense repeats based on the last paid date.
        ''' For example, if its a monthly expense that's due on the first of the month, but was last paid on the fifth of the month, then next month it will be due on the fifth.
        ''' </summary>
        <DescriptionAttribute("Last Paid Date")>
        LASTDATE

    End Enum

    ''' <summary>
    ''' Contains a list of the configured groups in the monthly expenses grid.
    ''' </summary>
    Public Enum MonthlyGridGroups

        ''' <summary>
        ''' Contains a list of any projected expenses that are due on or before today's date.
        ''' </summary>
        <DescriptionAttribute("Past Due Expenses")>
        PASTDUEEXPENSES = 0

        ''' <summary>
        ''' Contains a list of any projected income records that are due on or before today's date.
        ''' </summary>
        <DescriptionAttribute("Past Due Income")>
        PASTDUEINCOME = 1

        ''' <summary>
        ''' Contains a list of any projected or paid expenses that are due in the selected month.
        ''' </summary>
        <DescriptionAttribute("Monthly Expenses")>
        MONTHLYEXPENSES = 2

        ''' <summary>
        ''' Contains a list of any projected or recieved income that are due in the selected month.
        ''' </summary>
        <DescriptionAttribute("Monthly Income")>
        MONTHLYINCOME = 3
    End Enum

#End Region

#Region "Public Functions"

    ''' <summary>
    ''' Build a new list of options from the specified enum type.
    ''' </summary>
    ''' <param name="ListType">The type of enum to build the list from.  The Enum should be part of this class.</param>
    ''' <returns>A list of ExpenseCategory objects populated from the specified Enum.</returns>
    Public Shared Function BuildList(ByVal ListType As Type) As List(Of ListOption)
        ' Create a new list object to return.
        Dim ReturnList As New List(Of ListOption)

        ' Loop through all of the options in the specified Enum and add them to the list.
        For Each item In System.Enum.GetValues(ListType)

            ' Add the enum code and description text to the list.
            ReturnList.Add(New ListOption With {
                        .Code = item.ToString(),
                        .DisplayName = GetDescription(item.GetType(), item.ToString())
                     })

        Next

        ' Return the generated list.
        Return ReturnList
    End Function

    ''' <summary>
    ''' Get the Description value associated with the specified enum option.
    ''' </summary>
    ''' <param name="ListType">The enum type the option belongs to.</param>
    ''' <param name="ListSelection">The name of the enum option to read the description for.</param>
    ''' <returns></returns>
    Public Shared Function GetDescription(ByVal ListType As Type, ByVal ListSelection As String) As String
        Dim fi As FieldInfo = ListType.GetField(ListSelection)
        Dim attr() As DescriptionAttribute =
                  DirectCast(fi.GetCustomAttributes(GetType(DescriptionAttribute),
                  False), DescriptionAttribute())

        Return attr(0).Description
    End Function
#End Region

End Class
