﻿' Project Name:  Budget
' Copyright 2018 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 23 June 2018

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class ListOption
    Private _DisplayName As String

    ''' <summary>
    ''' Contains the display name of this list item.
    ''' </summary>
    ''' <returns></returns>
    Public Property DisplayName() As String
        Get
            Return _DisplayName
        End Get
        Set(ByVal value As String)
            _DisplayName = value
        End Set
    End Property

    Private _Code As String

    ''' <summary>
    ''' Contains the code that is used to represent this list item.
    ''' </summary>
    ''' <returns></returns>
    Public Property Code() As String
        Get
            Return _Code
        End Get
        Set(ByVal value As String)
            _Code = value
        End Set
    End Property
End Class
