﻿' Project Name:  Budget
' Copyright 2018 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 23 June 2018

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class XmlData

#Region "Properties"

    ''' <summary>
    ''' Contains all data in the Budget.xml file.
    ''' </summary>
    Private _Data As New DataSet("Budget")

    ''' <summary>
    ''' Contains the path and file name for the Budget.xml file.
    ''' </summary>
    Private _XmlFile As String = String.Empty

    ''' <summary>
    ''' Contains the path and file name to the schema for the Budget.xml file.
    ''' </summary>
    Private _XmlSchema As String = String.Empty

    ''' <summary>
    ''' Contains the configuration for all existing recurring expenses, loaded from the Budget.xml file.
    ''' </summary>
    Private _RecurringExpenses As List(Of ExpenseRecord)

    ''' <summary>
    ''' Contains the configuration for all existing recurring expenses, loaded from the Budget.xml file.
    ''' </summary>
    Public ReadOnly Property RecurringExpenses As List(Of ExpenseRecord)
        Get

            If Me._RecurringExpenses Is Nothing Then
                ' Load all of the configurations for recurring expenses from the xml file.
                Me._RecurringExpenses = New List(Of ExpenseRecord)
                Me.LoadRecurringExpenses()
            End If

            Return Me._RecurringExpenses

        End Get
    End Property

    ''' <summary>
    ''' Contains the payment history for all of the recurring expenses, loaded from the Budget.xml file.
    ''' </summary>
    Private _ExpensesHistory As List(Of HistoryRecord)

    ''' <summary>
    ''' Contains the payment history for all of the recurring expenses, loaded from the Budget.xml file.
    ''' </summary>
    Public ReadOnly Property ExpensesHistory As List(Of HistoryRecord)
        Get

            If Me._ExpensesHistory Is Nothing Then
                ' Load all the paid expenses history from the xml file.
                Me._ExpensesHistory = New List(Of HistoryRecord)
                Me.LoadPaidExpenseHistory()
            End If

            Return Me._ExpensesHistory

        End Get
    End Property

    ''' <summary>
    ''' Contains any notes the user has written on the sidebar.
    ''' </summary>
    Private _Notes As String

    ''' <summary>
    ''' Contains any notes the user has written on the sidebar.
    ''' </summary>
    Public Property Notes() As String

        Get
            Return Me._Notes
        End Get

        Set(ByVal value As String)
            Me._Notes = value
        End Set

    End Property

#End Region

#Region "Load Data Logic"

    ''' <summary>
    ''' Create a new instance of the XmlData Class.
    ''' </summary>
    Sub New()

        ' Set up the file names.
        Me._XmlFile = Application.StartupPath & "\Data\Budget.xml"
        Me._XmlSchema = Application.StartupPath & "\Data\Budget.xsd"

        ' Load all saved data from the Budget.xml file.
        Me.LoadData()

        ' Load any existing notes from the xml file
        Me.LoadNotes()

    End Sub

    ''' <summary>
    ''' Loads all previously saved data from the Budget.xml file.
    ''' </summary>
    Private Sub LoadData()

        ' Load all saved data from the .xml file.
        Me._Data.ReadXmlSchema(Me._XmlSchema)
        Me._Data.ReadXml(Me._XmlFile)

    End Sub

    ''' <summary>
    ''' Populates the RecurringExpenses with all of the recurring expenses from the Budget.xml file.
    ''' </summary>
    Private Sub LoadRecurringExpenses()

        ' Load each recurring expense record from the xml file into an in memory list for easier access.
        For Each Expense As DataRow In Me._Data.Tables(Options.Tables.Expense.ToString).Rows
            Dim NewExpense As New ExpenseRecord
            With NewExpense
                .ExpenseAmount = Expense.Item("ExpenseAmount").ToString()
                .ExpenseDescription = Expense.Item("ExpenseDescription").ToString()
                If Expense.Item("ExpenseEndDate") IsNot DBNull.Value Then
                    .ExpenseEndDate = Expense.Item("ExpenseEndDate").ToString()
                End If
                .ExpenseId = Expense.Item("ExpenseId").ToString()
                If Expense.Item("ExpenseLastPaidDate") IsNot DBNull.Value Then
                    .ExpenseLastPaidDate = Expense.Item("ExpenseLastPaidDate").ToString()
                End If
                If Expense.Item("ExpenseNextDueDate") IsNot DBNull.Value Then
                    .ExpenseNextDueDate = Expense.Item("ExpenseNextDueDate").ToString()
                End If
                .ExpenseNextRecurranceTypeCode = Expense.Item("ExpenseNextRecurranceType").ToString()
                .ExpenseRecurranceInterval = Expense.Item("ExpenseRecurranceInterval").ToString()
                .ExpenseRecurranceTypeCode = Expense.Item("ExpenseRecurranceType").ToString()
                .ExpenseStartDate = Expense.Item("ExpenseStartDate").ToString()
                .ExpenseType = Expense.Item("ExpenseType").ToString()
            End With
            Me._RecurringExpenses.Add(NewExpense)
        Next

    End Sub

    ''' <summary>
    ''' Populates the ExpensesHistory with the history data from the Budget.xml file.
    ''' </summary>
    Private Sub LoadPaidExpenseHistory()

        ' Load each expense history record from the xml file into an in memory list for easier access.
        For Each ExpenseHistory As DataRow In Me._Data.Tables(Options.Tables.ExpenseHistory.ToString).Rows
            Dim NewHistory As New HistoryRecord
            With NewHistory
                .ExpenseActualDate = ExpenseHistory.Item("ExpenseActualDate").ToString()
                .ExpenseEstimatedDate = ExpenseHistory.Item("ExpenseEstimatedDate").ToString()
                .ExpenseHistoryId = ExpenseHistory.Item("ExpenseHistoryId").ToString()
                .ExpenseId = ExpenseHistory.Item("ExpenseId").ToString()
                .ExpensePaidAmount = ExpenseHistory.Item("ExpensePaidAmount").ToString()
                .ExpenseEstimatedAmount = ExpenseHistory.Item("ExpenseEstimatedAmount").ToString()
                .ExpenseMemo = ExpenseHistory.Item("ExpenseMemo").ToString()
            End With
            Me._ExpensesHistory.Add(NewHistory)
        Next

    End Sub

    ''' <summary>
    ''' Populates Notes with any previously saved notes from the Budget.xml file.
    ''' </summary>
    Private Sub LoadNotes()

        ' Check and see if saved notes exist.
        If Me._Data.Tables(Options.Tables.Notes.ToString).Rows.Count > 0 Then

            ' If any saved notes were found, load the first one.  There should never be more then one.
            Dim RowNotes As DataRow = Me._Data.Tables(Options.Tables.Notes.ToString).Rows(0)
            Me.Notes = RowNotes.Item("NoteText").ToString()

        End If

    End Sub

#End Region

#Region "Save Data Logic"

    ''' <summary>
    ''' Saves all configured data to the Budget.xml file.
    ''' </summary>
    Public Sub SaveData()

        ' Update _Data with recurring expenses.
        SaveRecurringExpenses()

        ' Update _Data with expense history records.
        SaveExpenseHistoryRecords()

        ' Update _Data with the current notes.
        SaveNotes()

        ' Write everything to the Budget.xml file.
        Me._Data.WriteXml(Application.StartupPath & "\Data\Budget.xml")

    End Sub

    ''' <summary>
    ''' Updates the _Data object with the current values from _RecurringExpenses.
    ''' </summary>
    Private Sub SaveRecurringExpenses()

        ' Loop through and create / update all the recurring expenses to the Expense table in the Budget.xml file.
        For Each Expense As ExpenseRecord In Me._RecurringExpenses

            ' Attempt to load the existing record for this expense.
            Dim ExistingExpense As DataRow() = Me._Data.Tables(Options.Tables.Expense.ToString).Select(String.Format("ExpenseId = {0}", Expense.ExpenseId))

            If ExistingExpense.Count > 0 Then
                ' If this expense is already in _Data, then update it with the current values.
                With Expense
                    ExistingExpense(0).Item("ExpenseAmount") = .ExpenseAmount
                    ExistingExpense(0).Item("ExpenseDescription") = .ExpenseDescription
                    If .ExpenseEndDate IsNot Nothing Then
                        ExistingExpense(0).Item("ExpenseEndDate") = .ExpenseEndDate
                    Else
                        ExistingExpense(0).Item("ExpenseEndDate") = DBNull.Value
                    End If
                    If .ExpenseLastPaidDate IsNot Nothing Then
                        ExistingExpense(0).Item("ExpenseLastPaidDate") = .ExpenseLastPaidDate
                    Else
                        ExistingExpense(0).Item("ExpenseLastPaidDate") = DBNull.Value
                    End If
                    If .ExpenseNextDueDate IsNot Nothing Then
                        ExistingExpense(0).Item("ExpenseNextDueDate") = .ExpenseNextDueDate
                    Else
                        ExistingExpense(0).Item("ExpenseNextDueDate") = DBNull.Value
                    End If
                    ExistingExpense(0).Item("ExpenseNextRecurranceType") = .ExpenseNextRecurranceTypeCode
                    ExistingExpense(0).Item("ExpenseRecurranceInterval") = .ExpenseRecurranceInterval
                    ExistingExpense(0).Item("ExpenseRecurranceType") = .ExpenseRecurranceTypeCode
                    ExistingExpense(0).Item("ExpenseStartDate") = .ExpenseStartDate
                    ExistingExpense(0).Item("ExpenseType") = .ExpenseType
                End With

            Else
                ' If this expense was not already in _Data, then create a new record
                Dim NewRow As DataRow = Me._Data.Tables(Options.Tables.Expense.ToString).NewRow()
                With Expense
                    NewRow.Item("ExpenseAmount") = .ExpenseAmount
                    NewRow.Item("ExpenseDescription") = .ExpenseDescription
                    If .ExpenseEndDate IsNot Nothing Then
                        NewRow.Item("ExpenseEndDate") = .ExpenseEndDate
                    Else
                        NewRow.Item("ExpenseEndDate") = DBNull.Value
                    End If
                    If .ExpenseLastPaidDate IsNot Nothing Then
                        NewRow.Item("ExpenseLastPaidDate") = .ExpenseLastPaidDate
                    Else
                        NewRow.Item("ExpenseLastPaidDate") = DBNull.Value
                    End If
                    If .ExpenseNextDueDate IsNot Nothing Then
                        NewRow.Item("ExpenseNextDueDate") = .ExpenseNextDueDate
                    Else
                        NewRow.Item("ArticleUrl") = DBNull.Value
                    End If
                    NewRow.Item("ExpenseNextRecurranceType") = .ExpenseNextRecurranceTypeCode
                    NewRow.Item("ExpenseRecurranceInterval") = .ExpenseRecurranceInterval
                    NewRow.Item("ExpenseRecurranceType") = .ExpenseRecurranceTypeCode
                    NewRow.Item("ExpenseStartDate") = .ExpenseStartDate
                    NewRow.Item("ExpenseType") = .ExpenseType

                    ' Update expense history items with new expense ids as ids can change from temp id to real id during save.
                    For Each ExpenseHistory In Me._ExpensesHistory.Where(Function(f) f.ExpenseId = Expense.ExpenseId).ToList()
                        ExpenseHistory.ExpenseId = NewRow.Item("ExpenseId").ToString()
                    Next

                    ' Update the ExpenseId in memory to the new id so we have the correct value to use with any history records that get made going forward.
                    .ExpenseId = NewRow.Item("ExpenseId").ToString()
                End With

                ' Add the new row to the Expense table in _Data
                Me._Data.Tables(Options.Tables.Expense.ToString).Rows.Add(NewRow)
            End If
        Next

    End Sub

    ''' <summary>
    ''' Updates _Data with the current values from _ExpensesHistory.
    ''' </summary>
    Private Sub SaveExpenseHistoryRecords()

        ' Loop through and create / update all the expense history records to the ExpenseHistory table in the Budget.xml file.
        For Each ExpenseHistory As HistoryRecord In Me._ExpensesHistory

            ' Attempt to load the existing record for this history entry.
            Dim ExistingHistory As DataRow() = Me._Data.Tables(Options.Tables.ExpenseHistory.ToString).Select(String.Format("ExpenseHistoryId = {0}", ExpenseHistory.ExpenseHistoryId))

            If ExistingHistory.Count > 0 Then
                ' If an existing history record exists, then update it with the current values.
                With ExpenseHistory
                    ExistingHistory(0).Item("ExpenseActualDate") = .ExpenseActualDate
                    ExistingHistory(0).Item("ExpenseEstimatedDate") = .ExpenseEstimatedDate
                    ExistingHistory(0).Item("ExpenseId") = .ExpenseId
                    ExistingHistory(0).Item("ExpensePaidAmount") = .ExpensePaidAmount
                    ExistingHistory(0).Item("ExpenseEstimatedAmount") = .ExpenseEstimatedAmount
                    ExistingHistory(0).Item("ExpenseMemo") = .ExpenseMemo
                End With

            Else
                ' If an existing history record did not exist, then add a new row to the ExpenseHistory table for this item.
                Dim NewRow As DataRow = Me._Data.Tables(Options.Tables.ExpenseHistory.ToString).NewRow()
                With ExpenseHistory
                    NewRow.Item("ExpenseActualDate") = .ExpenseActualDate
                    NewRow.Item("ExpenseEstimatedDate") = .ExpenseEstimatedDate
                    NewRow.Item("ExpenseId") = .ExpenseId
                    NewRow.Item("ExpensePaidAmount") = .ExpensePaidAmount
                    NewRow.Item("ExpenseEstimatedAmount") = .ExpenseEstimatedAmount
                    NewRow.Item("ExpenseMemo") = .ExpenseMemo

                    ' Update the ExpenseHistoryId to the new id in memory in case the user tries to edit this later.
                    .ExpenseHistoryId = NewRow.Item("ExpenseHistoryId").ToString()
                End With

                ' Add the new row to the table.
                Me._Data.Tables(Options.Tables.ExpenseHistory.ToString).Rows.Add(NewRow)
            End If
        Next

    End Sub

    ''' <summary>
    ''' Update _Data with any user generated notes.
    ''' </summary>
    Private Sub SaveNotes()

        If Me._Data.Tables(Options.Tables.Notes.ToString).Rows.Count = 0 Then
            ' If no rows exist in the Notes table, add a new one and store the current notes.
            Dim RowNotes As DataRow = Me._Data.Tables(Options.Tables.Notes.ToString).NewRow()
            RowNotes.Item("NoteText") = Me.Notes
            Me._Data.Tables(Options.Tables.Notes.ToString).Rows.Add(RowNotes)

        Else
            ' If a notes row does exist then update it with the current notes text.
            Dim RowNotes As DataRow = Me._Data.Tables(Options.Tables.Notes.ToString).Rows(0)
            RowNotes.Item("NoteText") = Me.Notes
        End If

    End Sub

    ''' <summary>
    ''' Update the details of an existing expense history record.
    ''' </summary>
    ''' <param name="ExpenseHistoryId">The ID of the history record.  
    ''' A positive number indicates this is an existing history record, a negative number indicates this is a calculated expense.</param>
    ''' <param name="AmountPaid">The actual amount paid for this history entry.</param>
    ''' <param name="DatePaid">The actual date paid for this history entry.</param>
    ''' <param name="ExpenseMemo">A short note or description about this specific payment.</param>
    ''' <returns>Returns true or false to indicate if the ExpenseNextDueDate needs to be updated as well.</returns>
    Public Function UpdateExpenseHistoryRecord(ByVal ExpenseHistoryId As Integer, ByVal AmountPaid As Decimal,
                                               ByVal DatePaid As Date, ByVal ExpenseMemo As String) As Boolean

        ' Indicates if the ExpenseLastPaidDate in the expense configuration was updated.
        '   If it was, then the Expense Next Due Date needs to be re-calculated as well.
        Dim ExpenseLastPaidDateUpdated As Boolean = False

        ' Load the existing history record.
        Dim ExpenseHistoryItem As HistoryRecord = Me._ExpensesHistory.Where(Function(e) e.ExpenseHistoryId = ExpenseHistoryId).FirstOrDefault()

        If ExpenseHistoryItem IsNot Nothing Then ' Safty check.  This should never not be nothing.

            ' Find the configuration for this expense history record.
            Dim Expense As ExpenseRecord = Me._RecurringExpenses.Where(Function(e) e.ExpenseId = ExpenseHistoryItem.ExpenseId).FirstOrDefault()

            ' If the last paid date on the configuration record is equal to the original date on the history record, 
            '   Then we're updating the most recent history record so the last paid date should be updated to match.
            If Expense.ExpenseLastPaidDate = ExpenseHistoryItem.ExpenseActualDate Then
                Expense.ExpenseLastPaidDate = DatePaid
                ExpenseLastPaidDateUpdated = True

            ElseIf Expense.ExpenseLastPaidDate < DatePaid Then
                ' If the last paid date is less then the date this history item is being set to paid on, then
                '   we should update the configuration to match.
                Expense.ExpenseLastPaidDate = DatePaid
                ExpenseLastPaidDateUpdated = True
            End If

            ' Update the history item with the new values.
            ExpenseHistoryItem.ExpensePaidAmount = AmountPaid
            ExpenseHistoryItem.ExpenseActualDate = DatePaid
            ExpenseHistoryItem.ExpenseMemo = ExpenseMemo

        End If

        ' Return true or false to indicate if the ExpenseNextDueDate needs to be updated as well.
        Return ExpenseLastPaidDateUpdated

    End Function

    ''' <summary>
    ''' Creates a new Expense History record from a projected expense record.
    ''' </summary>
    ''' <param name="ExpenseHistoryItem">The projected expense record to create the history record from.</param>
    ''' <param name="AmountPaid">The actual amount paid for this history entry.</param>
    ''' <param name="DatePaid">The actual date paid for this history entry.</param>
    ''' <param name="ExpenseMemo">A short note or description about this specific payment.</param>
    ''' <returns>Returns true or false to indicate if the ExpenseNextDueDate needs to be updated as well.</returns>
    Public Function CreateExpenseHistoryFromProjectedExpenseRecord(ByVal ExpenseHistoryItem As HistoryRecord,
                                                                   ByVal AmountPaid As Decimal, ByVal DatePaid As Date, ByVal ExpenseMemo As String) As Boolean

        ' Indicates if the ExpenseLastPaidDate in the expense configuration was updated.
        '   If it was, then the Expense Next Due Date needs to be re-calculated as well.
        Dim ExpenseLastPaidDateUpdated As Boolean = False

        ' Update the projected expense with the new values.
        ExpenseHistoryItem.ExpensePaidAmount = AmountPaid
        ExpenseHistoryItem.ExpenseActualDate = DatePaid
        ExpenseHistoryItem.ExpenseMemo = ExpenseMemo

        ' Load the corresponding configuration record for this history item.
        Dim Expense As ExpenseRecord = Me._RecurringExpenses.Where(Function(e) e.ExpenseId = ExpenseHistoryItem.ExpenseId).FirstOrDefault()

        ' If this expense has never been paid before, then set it's last paid date to this date.
        If Expense.ExpenseLastPaidDate Is Nothing Then
            Expense.ExpenseLastPaidDate = DatePaid
            ExpenseLastPaidDateUpdated = True

        ElseIf Expense.ExpenseLastPaidDate < DatePaid Then
            ' If this expense has been paid before, but before this paid date, then update the last paid date to this date.
            Expense.ExpenseLastPaidDate = DatePaid
            ExpenseLastPaidDateUpdated = True
        End If

        ' Move this item from the projected expenses list to the history list.
        Me._ExpensesHistory.Add(ExpenseHistoryItem)

        ' Return true or false to indicate if the ExpenseNextDueDate needs to be updated as well.
        Return ExpenseLastPaidDateUpdated

    End Function

    ''' <summary>
    ''' Create a new recurring expense in the system.
    ''' </summary>
    ''' <param name="NewExpense">A new ExpenseRecord object with the configurations for the recurring expense.</param>
    Public Sub AddNewExpense(ByVal NewExpense As ExpenseRecord)

        ' Figure out what the current lowest id is for the existing recurring expenses and then set the id on this expense to the next lowest negative number.
        Dim ExistingExpense As ExpenseRecord = Me._RecurringExpenses.OrderBy(Function(f) f.ExpenseId).FirstOrDefault
        If ExistingExpense Is Nothing OrElse ExistingExpense.ExpenseId = 1 Then
            NewExpense.ExpenseId = -1
        Else
            NewExpense.ExpenseId = ExistingExpense.ExpenseId - 1
        End If

        ' default next due date to expense start date.
        NewExpense.ExpenseNextDueDate = NewExpense.ExpenseStartDate

        ' Add the new expense to the list.
        Me._RecurringExpenses.Add(NewExpense)

        ' Save the new expense to disk.
        Me.SaveData()

    End Sub

    ''' <summary>
    ''' Update the details of an existing recurring expense in the system.
    ''' </summary>
    ''' <param name="ExpenseDetails">A new ExpenseRecord object with the configurations for the recurring expense.</param>
    ''' <param name="NextDueDate">The date this expense is next due on.</param>
    Public Sub UpdateExpense(ByVal ExpenseDetails As ExpenseRecord, ByRef NextDueDate As Date?)

        ' Load the details of the expense recurring expense.
        Dim ExistingExpense As ExpenseRecord = Me._RecurringExpenses.Where(Function(e) e.ExpenseId = ExpenseDetails.ExpenseId).FirstOrDefault()

        ' Updatae the recurring expense with the received configuration.
        With ExistingExpense
            .ExpenseAmount = ExpenseDetails.ExpenseAmount
            .ExpenseStartDate = ExpenseDetails.ExpenseStartDate
            .ExpenseRecurranceTypeCode = ExpenseDetails.ExpenseRecurranceTypeCode
            .ExpenseRecurranceInterval = ExpenseDetails.ExpenseRecurranceInterval
            .ExpenseNextRecurranceTypeCode = ExpenseDetails.ExpenseNextRecurranceTypeCode
            .ExpenseEndDate = ExpenseDetails.ExpenseEndDate
            .ExpenseDescription = ExpenseDetails.ExpenseDescription
            .ExpenseType = ExpenseDetails.ExpenseType
            .ExpenseNextDueDate = NextDueDate
        End With

        ' Save all changes.
        Me.SaveData()

    End Sub

    ''' <summary>
    ''' Delete the specified Recurring Expense and any associated data.
    ''' </summary>
    ''' <param name="ExpenseId">The ID of the Expense record to delete.</param>
    Public Sub DeleteRecurringExpense(ByVal ExpenseId As Integer)

        ' Find the expense record to be deleted.
        Dim ExistingExpense As ExpenseRecord = Me._RecurringExpenses.Where(Function(e) e.ExpenseId = ExpenseId).FirstOrDefault()

        ' If the record exists, then delete it and any associated data.
        If ExistingExpense IsNot Nothing Then

            ' Delete any associated payment records.
            Me.DeletePaymentHistory(ExpenseId)

            ' Delete the actual expense record
            Dim ExpenseRow As DataRow() = Me._Data.Tables(Options.Tables.Expense.ToString).Select("ExpenseId = " & ExpenseId)
            Me._Data.Tables(Options.Tables.Expense.ToString).Rows.Remove(ExpenseRow(0))

            ' Remove the record from memory.
            Me._RecurringExpenses.Remove(ExistingExpense)

            ' Save all changes.
            Me.SaveData()
        End If

    End Sub

    ''' <summary>
    ''' Delete all of the history records associated to the supplied Expense ID.
    ''' </summary>
    ''' <param name="ExpenseId">The ID of the Expense to delete the history of</param>
    Private Sub DeletePaymentHistory(ByVal ExpenseId As Integer)

        ' Build a list of payment records associated with the supplied expense id
        Dim PaymentRecords As List(Of HistoryRecord) = Me._ExpensesHistory.Where(Function(e) e.ExpenseId = ExpenseId).ToList()

        ' Loop through each payment record and remove them.
        For Each Record As HistoryRecord In PaymentRecords

            ' Remove the associated record from the xml.
            Dim PaymentRow As DataRow() = Me._Data.Tables(Options.Tables.ExpenseHistory.ToString).Select("ExpenseHistoryId = " & Record.ExpenseHistoryId)
            Me._Data.Tables(Options.Tables.ExpenseHistory.ToString).Rows.Remove(PaymentRow(0))

            ' Remove the record from memory
            Me._ExpensesHistory.Remove(Record)
        Next

    End Sub

#End Region

End Class
