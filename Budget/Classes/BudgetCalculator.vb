﻿' Project Name:  Budget
' Copyright 2018 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 23 June 2018

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class BudgetCalculator

#Region "Properties"

    Private _BudgetData As XmlData

    ''' <summary>
    ''' Contains all data in the Budget.xml file.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property BudgetData() As XmlData
        Get
            Return _BudgetData
        End Get
    End Property

    ''' <summary>
    ''' Contains a list of expense types (Daily, Weekly, Monthly, Yearly).
    ''' </summary>
    Private _ExpenseRecurranceTypes As List(Of ListOption)

    ''' <summary>
    ''' Contains a list of expense types (Daily, Weekly, Monthly, Yearly).
    ''' </summary>
    Public ReadOnly Property ExpenseRecurranceTypes As List(Of ListOption)
        Get
            Return _ExpenseRecurranceTypes
        End Get
    End Property

    ''' <summary>
    ''' Contains a list of available recurrance options (Recur based on Initial Due Date, Recur based on Last Paid Date)
    ''' </summary>
    Private _ExpenseCalculationTypes As List(Of ListOption)

    ''' <summary>
    ''' Contains a list of available recurrance options (Recur based on Initial Due Date, Recur based on Last Paid Date)
    ''' </summary>
    Public ReadOnly Property ExpenseCalculationTypes As List(Of ListOption)
        Get
            Return _ExpenseCalculationTypes
        End Get
    End Property

    ''' <summary>
    ''' Contains a temporary list of calculated upcoming expenses that have not yet been paid.
    ''' </summary>
    Private _ProjectedExpenses As New List(Of HistoryRecord)

    ''' <summary>
    ''' Contains a temporary list of calculated upcoming expenses that have not yet been paid.
    ''' </summary>
    Public ReadOnly Property ProjectedExpenses As List(Of HistoryRecord)
        Get
            Return _ProjectedExpenses
        End Get
    End Property

    ''' <summary>
    ''' Indicates how far foward ProjectedExpenses have been calculated until.  Will be set to the first of the month.  
    ''' Expenses are calculated for all months before this month.
    ''' </summary>
    Private LoadedMonth As Date? = Nothing

#End Region

#Region "Load Data Logic"

    ''' <summary>
    ''' Create a new instance of the CalculationManager Class.
    ''' </summary>
    Sub New()

        ' Load all saved data from the Budget.xml file.
        Me._BudgetData = New XmlData

        ' Populate all of the global lists with configured and saved data.
        LoadExpenseData()

    End Sub

    ''' <summary>
    ''' Loads configurated data and any saved data into memory.
    ''' </summary>
    Private Sub LoadExpenseData()

        ' Generate a list of available expense types.
        Me._ExpenseRecurranceTypes = Options.BuildList(GetType(Options.RecurranceType))

        ' Generate a list of available recurrance types.
        Me._ExpenseCalculationTypes = Options.BuildList(GetType(Options.CalculationType))

    End Sub

#End Region

#Region "Save Data Logic"

    ''' <summary>
    ''' Save a new record to the expense history table or update an existing item.
    ''' </summary>
    ''' <param name="ExpenseHistoryId">The ID of the history record.  
    ''' A positive number indicates this is an existing history record, a negative number indicates this is a calculated expense.</param>
    ''' <param name="AmountPaid">The actual amount paid for this history entry.</param>
    ''' <param name="DatePaid">The actual date paid for this history entry.</param>
    ''' <param name="ExpenseMemo">A short note or description about this specific payment.</param>
    Public Sub SaveExpenseHistory(ByVal ExpenseHistoryId As Integer, ByVal AmountPaid As Decimal, ByVal DatePaid As Date, ByVal ExpenseMemo As String)

        ' Indicates if the ExpenseLastPaidDate in the expense configuration was updated.
        '   If it was, then the Expense Next Due Date needs to be re-calculated as well.
        Dim ExpenseLastPaidDateUpdated As Boolean = False

        ' Indicates if the existing projections for this expense are no longer valid.
        Dim RecalculateFutureExpenses As Boolean = False
        Dim ExpenseConfiguration As ExpenseRecord = Nothing

        ' If this history id is positive, then we're editing an existing expense history record.
        If ExpenseHistoryId > 0 Then
            ExpenseLastPaidDateUpdated = Me._BudgetData.UpdateExpenseHistoryRecord(ExpenseHistoryId, AmountPaid, DatePaid, ExpenseMemo)

            ' Load the configuration for this item to determine if it's projected expenses need to be re-calculated.
            Dim ExpenseHistoryRecord = Me._BudgetData.ExpensesHistory.Where(Function(e) e.ExpenseHistoryId = ExpenseHistoryId).FirstOrDefault()
            ExpenseConfiguration = Me._BudgetData.RecurringExpenses.Where(Function(E) E.ExpenseId = ExpenseHistoryRecord.ExpenseId).FirstOrDefault()
            If ExpenseConfiguration.ExpenseNextRecurranceTypeCode = Options.CalculationType.LASTDATE.ToString() Then
                RecalculateFutureExpenses = True
            End If

        Else
            ' If the history id is negative, then we're marking a projected expense off as paid.

            ' Load the calculated record from the projected expenses list.
            Dim ExpenseHistoryItem As HistoryRecord = Me._ProjectedExpenses.Where(Function(e) e.ExpenseHistoryId = ExpenseHistoryId).FirstOrDefault()

            If ExpenseHistoryItem IsNot Nothing Then ' Safty check.  This should never happen.

                ' Create a new expense history item based on the projected expense record and the user supplied values.
                ExpenseLastPaidDateUpdated = Me._BudgetData.CreateExpenseHistoryFromProjectedExpenseRecord(ExpenseHistoryItem, AmountPaid, DatePaid, ExpenseMemo)

                ' Remove this item from the projected expenses.
                Me._ProjectedExpenses.Remove(ExpenseHistoryItem)

                ' Load the configuration for this item to determine if it's projected expenses need to be re-calculated.
                ExpenseConfiguration = Me._BudgetData.RecurringExpenses.Where(Function(E) E.ExpenseId = ExpenseHistoryItem.ExpenseId).FirstOrDefault()
                If ExpenseConfiguration.ExpenseNextRecurranceTypeCode = Options.CalculationType.LASTDATE.ToString() Then
                    RecalculateFutureExpenses = True
                End If

            End If
        End If

            ' If we updated ExpenseLastPaidDate on the recurring expense configuration, then we also need to calculate and update the estimated next due date.
            If ExpenseLastPaidDateUpdated Then
            UpdateExpenseNextDueDate(ExpenseHistoryId)
        End If

        ' Save all changes.  This will flip any negative primary keys in the history table to positive numbers.
        Me._BudgetData.SaveData()

        ' Marking an entry as paid or updating an existing history entry can screw up existing calculations so re-calculate if necessary.
        If RecalculateFutureExpenses Then
            Me.CalculateFutureExpenses(Me.LoadedMonth, ExpenseConfiguration, True)
        End If

    End Sub

    ''' <summary>
    ''' Updates the next due date for the specified expense based on it's configuration.
    ''' </summary>
    ''' <param name="ExpenseHistoryId">The ID of the history record.</param>
    Private Sub UpdateExpenseNextDueDate(ByVal ExpenseHistoryId As Integer)

        ' This will be used to store the next due date for this expense.
        Dim ExpenseDueDate As Date

        ' Load the existing history record.
        Dim ExpenseHistoryItem As HistoryRecord = Me._BudgetData.ExpensesHistory.Where(Function(e) e.ExpenseHistoryId = ExpenseHistoryId).FirstOrDefault()

        If ExpenseHistoryItem IsNot Nothing Then ' Safty check.  This should never not be nothing.

            ' Find the configuration for this expense history record.
            Dim Expense As ExpenseRecord = Me._BudgetData.RecurringExpenses.Where(Function(e) e.ExpenseId = ExpenseHistoryItem.ExpenseId).FirstOrDefault()

            ' First, figure out when this expense was last paid on from the history item.
            '   If we calculate the next date based on initial start date, then use the Estimated Date.
            '   If we calculate the next due date based on the last paid date, then use the actual date.
            If Expense.ExpenseNextRecurranceTypeCode = Options.CalculationType.STARTDATE.ToString() Then
                ExpenseDueDate = ExpenseHistoryItem.ExpenseEstimatedDate
            Else
                ExpenseDueDate = ExpenseHistoryItem.ExpenseActualDate
            End If

            ' Take the last due date, and then increase it by the configured amount of days / months / years.
            ExpenseDueDate = CalculateNextDueDate(ExpenseDueDate, Expense.ExpenseRecurranceTypeCode, Expense.ExpenseRecurranceInterval)

            ' Update the next due date on the expense record.
            If Expense.ExpenseEndDate IsNot Nothing Then
                If ExpenseDueDate > Expense.ExpenseEndDate Then
                    ' If End Date is set, and the next due date would be after the end date, then we're done with this expense,
                    '  Set the next due date to nothing, otherwise set it to the calculated due date.
                    Expense.ExpenseNextDueDate = Nothing
                Else
                    Expense.ExpenseNextDueDate = ExpenseDueDate
                End If
            Else
                Expense.ExpenseNextDueDate = ExpenseDueDate
            End If
        End If

    End Sub

    ''' <summary>
    ''' Create a new recurring expense in the system.
    ''' </summary>
    ''' <param name="NewExpense">A new ExpenseRecord object with the configurations for the recurring expense.</param>
    Public Sub AddNewExpense(ByVal NewExpense As ExpenseRecord)

        ' Add the new expense to the budget data.
        Me._BudgetData.AddNewExpense(NewExpense)

        ' Run calculations for the new item so that we have projected payments available to display.
        Me.CalculateFutureExpenses(Me.LoadedMonth, NewExpense, True)

    End Sub

    ''' <summary>
    ''' Update the details of an existing recurring expense in the system.
    ''' </summary>
    ''' <param name="ExpenseDetails">A new ExpenseRecord object with the configurations for the recurring expense.</param>
    Public Sub UpdateExpense(ByVal ExpenseDetails As ExpenseRecord)

        ' Load the details of the expense recurring expense.
        Dim ExistingExpense As ExpenseRecord = Me._BudgetData.RecurringExpenses.Where(Function(e) e.ExpenseId = ExpenseDetails.ExpenseId).FirstOrDefault()

        Dim NextDueDate As Date? = Nothing
        ' Recalculate the next due date
        If ExistingExpense.ExpenseLastPaidDate Is Nothing Then
            NextDueDate = ExistingExpense.ExpenseStartDate
        Else
            NextDueDate = CalculateNextDueDate(ExistingExpense.ExpenseLastPaidDate, ExistingExpense.ExpenseRecurranceTypeCode, ExpenseDetails.ExpenseRecurranceInterval)
        End If

        ' If End Date is set and is after the next due date, then null out the next due date.
        If ExpenseDetails.ExpenseEndDate IsNot Nothing AndAlso NextDueDate IsNot Nothing AndAlso NextDueDate > ExpenseDetails.ExpenseEndDate Then
            NextDueDate = Nothing
        End If

        ' Save changes to the recurring expense configuration.
        Me._BudgetData.UpdateExpense(ExpenseDetails, NextDueDate)

        ' Re-run calcuations for this item to make sure all the projected data is current (amount, end date, etc).
        Me.CalculateFutureExpenses(Me.LoadedMonth, Me._BudgetData.RecurringExpenses.Where(Function(e) e.ExpenseId = ExpenseDetails.ExpenseId).FirstOrDefault(), True)

    End Sub

    ''' <summary>
    ''' Delete the specified Recurring Expense and any associated data.
    ''' </summary>
    ''' <param name="ExpenseId">The ID of the Expense record to delete.</param>
    Public Sub DeleteRecurringExpense(ByVal ExpenseId As Integer)

        ' Delete the recurring expense.
        Me._BudgetData.DeleteRecurringExpense(ExpenseId)

        ' Remove any projected expenses associated with this item
        Me._ProjectedExpenses.RemoveAll(Function(e) e.ExpenseId = ExpenseId)

    End Sub

#End Region

#Region "Calculation Logic"

    ''' <summary>
    ''' Clears all calculated data.
    ''' </summary>
    Public Sub ClearCalculations()

        Me.LoadedMonth = Nothing
        Me._ProjectedExpenses.Clear()

    End Sub

    ''' <summary>
    ''' Calculates projected expenses forward to the specified month.
    ''' </summary>
    ''' <param name="EndMonth">The month to stop calculating at.  
    ''' Should be set to the first of the month, after the month that you want to generate expenses for (ex: If calculating expenses for June, set to July 1st).
    ''' </param>
    Public Sub CalculateAllFutureExpenses(ByVal EndMonth As Date)

        ' Figure out what the current month is
        Dim CurrentMonth As Date = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1)

        ' If EndMonth is less then the current month, use the current month instead to insure any past due expenses get populated into memory.
        If EndMonth < CurrentMonth Then
            EndMonth = CurrentMonth
        End If

        ' If we have not calculated data up to the end of the supplied MONTH, then calculate each expense up until that date.
        '   Othewise calculations were previously run and there's nothing more to do here.
        If Me.LoadedMonth Is Nothing OrElse Me.LoadedMonth < EndMonth Then

            ' Loop through each of the configured expenses and calculate any expenses that fall between their last paid date and EndMonth.
            For Each Expense As ExpenseRecord In Me._BudgetData.RecurringExpenses

                ' Project the current expense forward until EndMonth is reached.
                Me.CalculateFutureExpenses(EndMonth, Expense)

            Next

            ' Calculations are complete.  Move LoadedMonth forward to track how far data has been calculated up to.
            Me.LoadedMonth = EndMonth
        End If

    End Sub

    ''' <summary>
    ''' Projects a single expense forward based on it's configuration in ExpenseRecord until EndMonth is reached.
    ''' </summary>
    ''' <param name="EndMonth">The date to stop calculating on.</param>
    ''' <param name="Expense">The recurring expense configuration to calculate.</param>
    ''' <param name="ResetProjections">Indicates if existing calculations should be removed.</param>
    Public Sub CalculateFutureExpenses(ByVal EndMonth As Date, ByVal Expense As ExpenseRecord, Optional ByVal ResetProjections As Boolean = False)

        ' If Reset Projections is true, then remove all existing calculations.
        If ResetProjections = True Then
            Me._ProjectedExpenses.RemoveAll(Function(e) e.ExpenseId = Expense.ExpenseId)
        End If

        ' Figure out the next id to use for any expenses that get calculated.
        '   All projected expenses have a negative id to distinguish from paid expenses that get saved to the Budget.xml file with a postive id.
        Dim NextProjectedExpenseId As Integer = -1
        If Me._ProjectedExpenses.Count > 0 Then
            NextProjectedExpenseId = Me._ProjectedExpenses.OrderBy(Function(e) e.ExpenseHistoryId).FirstOrDefault().ExpenseHistoryId - 1
        End If

        ' This will be the the next calculated date for this expense.  
        '   Setting to MinValue so that we can loop and generate multiple records from this expense if needed.
        Dim ExpenseDueDate As Date = Date.MinValue

        ' Loop until the next calculated date is after the stop date.
        While ExpenseDueDate < EndMonth

            ' Figure out when this expense was last paid on.
            Dim LastCalculatedRecord = Me._ProjectedExpenses.Where(Function(e) e.ExpenseId =
                                                                       Expense.ExpenseId).OrderBy(Function(f) f.ExpenseEstimatedDate).LastOrDefault()

            If Expense.ExpenseLastPaidDate Is Nothing AndAlso LastCalculatedRecord Is Nothing Then
                ' If this expense has never been paid, and we haven't already calculated due dates, then it's due date is it's start date.  
                '    No further calculations needed.
                ExpenseDueDate = Expense.ExpenseStartDate

            Else
                ' If this expense either has been paid, or has already been calculated then get the last due date to use as a starting value.
                If Expense.ExpenseNextRecurranceTypeCode = Options.CalculationType.STARTDATE.ToString() Then
                    ' If we're calculating based on initial start date, then the last due date would be the EstimatedDate,
                    '   either from the paid expenses in the history table or the last calculated date, whichever is higher.
                    If LastCalculatedRecord Is Nothing Then
                        ExpenseDueDate = Me._BudgetData.ExpensesHistory.Where(Function(e) e.ExpenseId =
                                                                       Expense.ExpenseId).OrderBy(Function(f) f.ExpenseActualDate).LastOrDefault().ExpenseEstimatedDate
                    Else
                        ExpenseDueDate = LastCalculatedRecord.ExpenseEstimatedDate
                    End If

                Else
                    ' If we're calculating based on last paid date, then the last due date would be the Actual Paid Date,
                    '   either from the paid expenses in the history table or the last calculated date, whichever is higher.
                    If LastCalculatedRecord Is Nothing Then
                        ExpenseDueDate = Me._BudgetData.ExpensesHistory.Where(Function(e) e.ExpenseId =
                                                                       Expense.ExpenseId).OrderBy(Function(f) f.ExpenseActualDate).LastOrDefault().ExpenseActualDate
                    Else
                        ExpenseDueDate = LastCalculatedRecord.ExpenseActualDate
                    End If
                End If

                ' Take the last due date, and then increase it by the configured amount of days / months / years.
                ExpenseDueDate = CalculateNextDueDate(ExpenseDueDate, Expense.ExpenseRecurranceTypeCode, Expense.ExpenseRecurranceInterval)
            End If

            ' If this expense has an end date, and this calculated date is after the end date, then we don't care about it.
            If Expense.ExpenseEndDate Is Nothing OrElse ExpenseDueDate <= Expense.ExpenseEndDate Then

                ' If this expense due date is after the end date that we're calculating for, then we don't care about it.
                If ExpenseDueDate < EndMonth Then

                    ' If this expense is due within the calculation period, then create a new record for it to display on the screen.
                    Dim ProjectedExpense As New HistoryRecord
                    With ProjectedExpense
                        .ExpenseEstimatedAmount = Expense.ExpenseAmount
                        .ExpenseEstimatedDate = ExpenseDueDate
                        .ExpenseId = Expense.ExpenseId
                        .ExpenseHistoryId = NextProjectedExpenseId
                        ' This expense hasn't been paid yet so default these to the estimated amount and projected due date.
                        .ExpensePaidAmount = Expense.ExpenseAmount
                        .ExpenseActualDate = ExpenseDueDate
                    End With
                    Me._ProjectedExpenses.Add(ProjectedExpense)
                    NextProjectedExpenseId -= 1
                End If

            ElseIf ExpenseDueDate > Expense.ExpenseEndDate Then
                ' If the next due date is after the end date, increase the due date to the max date so that the loop will move onto the next expense.
                ExpenseDueDate = Date.MaxValue
            End If
        End While
    End Sub

    ''' <summary>
    ''' Calculates the next due date based on the last due date, the recurrance type and the interval.
    ''' </summary>
    ''' <param name="LastDueDate">The last date this item was due on.</param>
    ''' <param name="RecurranceTypeCode">How often this item recurs (DAILY, WEEKLY, etc)</param>
    ''' <param name="Interval">The interval that this item recurs at</param>
    ''' <returns>A Date that contains the next due date for this item.</returns>
    Private Function CalculateNextDueDate(ByVal LastDueDate As Date, ByVal RecurranceTypeCode As String, Interval As Integer) As Date

        ' ExpenseDueDate will contain the next due date.  Set it to the last due date to start.
        Dim ExpenseDueDate As Date = LastDueDate

        ' Increase expense due date based on the recurrance type and interval.
        If RecurranceTypeCode = Options.RecurranceType.DAILY.ToString() Then
            ExpenseDueDate = ExpenseDueDate.AddDays(Interval)
        ElseIf RecurranceTypeCode = Options.RecurranceType.WEEKLY.ToString() Then
            ExpenseDueDate = ExpenseDueDate.AddDays(Interval * 7)
        ElseIf RecurranceTypeCode = Options.RecurranceType.MONTHLY.ToString() Then
            ExpenseDueDate = ExpenseDueDate.AddMonths(Interval)
        ElseIf RecurranceTypeCode = Options.RecurranceType.YEARLY.ToString() Then
            ExpenseDueDate = ExpenseDueDate.AddYears(Interval)
        ElseIf RecurranceTypeCode = Options.RecurranceType.LASTDAYOFMONTH.ToString() Then
            ' For Last Day Of the Month, take the date it was last due on and go to the first of that month.
            '   Then go forward two months, then back one day to get to the last day of the next month.
            Dim TempDate As New Date(ExpenseDueDate.Year, ExpenseDueDate.Month, 1)
            TempDate = TempDate.AddMonths(2)
            TempDate = TempDate.AddDays(-1)
            ExpenseDueDate = TempDate
        End If

        ' Return the calculated date.
        Return ExpenseDueDate

    End Function

#End Region

End Class
