﻿' Project Name:  Budget
' Copyright 2018 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 23 June 2018

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Imports System.ComponentModel

Public Class BudgetMain

#Region "Properties"

    ''' <summary>
    ''' Handles all expense data and calcuations.
    ''' </summary>
    Public ExpenseCalcuator As New BudgetCalculator()

    ''' <summary>
    ''' Indicates what month is currently being displayed in the UI.  Should be set to the first of the month.
    ''' </summary>
    Private DisplayMonth As Date

#End Region

#Region "Main Form Control Code"

    ''' <summary>
    ''' Loads all saved data and performs any necessary initial setup.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub BudgetMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ' Load list of existing recurring expenses onto the screen.
        Me.LoadExpenseList()

        ' Load list of existing recurring incomes onto the screen.
        Me.LoadIncomeList()

        ' Create the groups in the monthly expenses list view.
        Me.LoadMonthlyExpenseGroups()

        ' Set display month to the beginning of the current month.
        Me.DisplayMonth = New Date(DateTime.Now.Year, DateTime.Now.Month, 1)

        ' Calculate and load all expenses for the current month.
        Me.ShowExpensesFor(DisplayMonth)

        ' Load any saved notes onto the screen.
        Me.txtNotes.Text = Me.ExpenseCalcuator.BudgetData.Notes

    End Sub

    ''' <summary>
    ''' This is called when the application is closing.  Saves all data to disk before quiting.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub BudgetMain_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing

        ' Save any changes before quiting.
        SaveNotesData()

    End Sub

#End Region

#Region "Monthly Expenses Controls Code"

    ''' <summary>
    ''' Edit a record in the "Expenses For MONTH" grid.  This is called when a grid item is double clicked on.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ListViewProjectedExpenses_DoubleClick(sender As Object, e As EventArgs) Handles ListViewProjectedExpenses.DoubleClick

        If ListViewProjectedExpenses.SelectedItems.Count > 0 Then ' Safty check, make sure something is actually selected.

            ' Create a new edit window.
            Dim EditExpenseHistory As New AddEditExpenseHistory
            EditExpenseHistory.ParentWindow = Me ' Used to pass data back to this window.
            EditExpenseHistory.Tag = ListViewProjectedExpenses.SelectedItems(0).Tag ' ID of the record that is being edited.

            ' Load the history record that's being edited.  This can be in one of two places.
            Dim SelectedExpense As HistoryRecord

            If ListViewProjectedExpenses.SelectedItems(0).Tag > 0 Then
                ' If the ID is positive, then we're editing an already paid expense.  Load data from Expense History.
                SelectedExpense = Me.ExpenseCalcuator.BudgetData.ExpensesHistory.Where(Function(f) f.ExpenseHistoryId =
                                                                                ListViewProjectedExpenses.SelectedItems(0).Tag).FirstOrDefault()

            Else
                ' If the ID is negative, then we're editing a projected expense to mark it off as paid.  Load data from Projected Expenses.
                SelectedExpense = Me.ExpenseCalcuator.ProjectedExpenses.Where(Function(f) f.ExpenseHistoryId =
                                                                                  ListViewProjectedExpenses.SelectedItems(0).Tag).FirstOrDefault()
            End If

            ' Populate the fields on the screen with the loaded data.
            EditExpenseHistory.txtAmount.Text = SelectedExpense.ExpensePaidAmount
            EditExpenseHistory.DateTimePaid.Value = New Date(SelectedExpense.ExpenseActualDate.Value.Year,
                                                             SelectedExpense.ExpenseActualDate.Value.Month,
                                                             SelectedExpense.ExpenseActualDate.Value.Day)
            EditExpenseHistory.txtExpenseMemo.Text = SelectedExpense.ExpenseMemo

            ' Show the dialog.
            EditExpenseHistory.ShowDialog()
        End If

    End Sub

    ''' <summary>
    ''' This is called when the Previous Month button is clicked.  It navigates backward one month.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub buttonPreviousMonth_Click(sender As Object, e As EventArgs) Handles buttonPreviousMonth.Click

        ' Decrease the value of DisplayMonth by one month, then call ShowExpensesFor to update the UI to this month.
        Me.DisplayMonth = Me.DisplayMonth.AddMonths(-1)
        ShowExpensesFor(DisplayMonth)

    End Sub

    ''' <summary>
    ''' This is called when the Next Month button is clicked.  It navigates forward one month.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub buttonNextMonth_Click(sender As Object, e As EventArgs) Handles buttonNextMonth.Click

        ' Increase the value of DisplayMonth by one month, then call ShowExpensesFor to update the UI to this month.
        Me.DisplayMonth = Me.DisplayMonth.AddMonths(1)
        ShowExpensesFor(DisplayMonth)

    End Sub

#End Region

#Region "Recurring Expenses Tree Control Code"

    ''' <summary>
    ''' Edit the selected recurring expense.  Called if an item in the list is double clicked.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub treeRecurringExpenses_DoubleClick(sender As Object, e As EventArgs) Handles treeRecurringExpenses.DoubleClick
        EditRecurringExpense(True)
    End Sub

#End Region

#Region "Recurring Income Tree Control Code"

    ''' <summary>
    ''' Edit the selected recurring income.  Called if an item in the list is double clicked.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub treeRecurringIncome_DoubleClick(sender As Object, e As EventArgs) Handles treeRecurringIncome.DoubleClick
        EditRecurringIncome(True)
    End Sub

#End Region

#Region "File Menu Options"

    ''' <summary>
    ''' Create a new recurring expense.  Called when the Add Expense item is clicked from the File menu.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AddExpenseToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddExpenseToolStripMenuItem.Click
        AddRecurringExpense()
    End Sub

    ''' <summary>
    ''' Edit an existing recurring expense.  Called when the Edit Expense item is clicked on from the File menu.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub EditExpenseToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EditExpenseToolStripMenuItem.Click
        EditRecurringExpense(False)
    End Sub

    ''' <summary>
    ''' Delete the selected recurring expense and any associated data.  Called from the File -> Delete Expense menu option.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub DeleteExpenseToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteExpenseToolStripMenuItem.Click
        DeleteRecurringExpense()
    End Sub

    ''' <summary>
    ''' Shows all records from the history table for the selected recurring expense.  Called from the Show Expense History menu option under the file menu.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ShowExpenseHistoryToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowExpenseHistoryToolStripMenuItem.Click
        ShowExpenseHistory()
    End Sub

    ''' <summary>
    ''' Create a new recurring income record.  Called from the Add Income menu option under the file menu.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AddIncomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddIncomeToolStripMenuItem.Click
        AddRecurringIncome()
    End Sub

    ''' <summary>
    ''' Edit an existing recurring income.  Called when the Edit Income item is clicked on from the File menu.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub EditIncomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EditIncomeToolStripMenuItem.Click
        EditRecurringIncome(False)
    End Sub

    ''' <summary>
    ''' Delete the selected recurring income and any associated data.  Called from the File -> Delete Income menu option.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub DeleteIncomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteIncomeToolStripMenuItem.Click
        DeleteRecurringIncome()
    End Sub

    ''' <summary>
    ''' Show all records from the history table for the selected recurring income.  Called from the Show Income History menu option under the File menu.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ShowIncomeHistoryToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowIncomeHistoryToolStripMenuItem.Click
        ShowIncomeHistory()
    End Sub

    ''' <summary>
    ''' Saves all data.  This is called when the Save menu item under the File menu is clicked.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub SaveToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SaveToolStripMenuItem.Click
        SaveNotesData()
    End Sub

    ''' <summary>
    ''' Exits the application.  Called when the Exit menu item in the File menu is clicked on.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        ' Exit the application.
        Application.Exit()
    End Sub

#End Region

#Region "Options Menu options"

    ''' <summary>
    ''' Displays the About dialog.  Called when the About menu item in the Options dialog is clicked on.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        ' Show the about window.
        Dim AboutWindow As New formAbout
        AboutWindow.ShowDialog()
    End Sub

#End Region

#Region "Recurring Expense Right Click Menu Options"

    ''' <summary>
    ''' Add a new recurring expense.  Called from the Right Click -> Add Expense menu option.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AddExpenseToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles AddExpenseToolStripMenuItem1.Click
        AddRecurringExpense()
    End Sub

    ''' <summary>
    ''' Edit the selected recurring expense.  Called from the Right Click -> Edit Expense menu option.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub EditExpenseToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles EditExpenseToolStripMenuItem1.Click
        EditRecurringExpense(False)
    End Sub

    ''' <summary>
    ''' Delete the selected recurring expense and any associated data.  Called from the Right Click -> Delete Expense menu option.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub DeleteExpenseToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles DeleteExpenseToolStripMenuItem1.Click
        DeleteRecurringExpense()
    End Sub

    ''' <summary>
    ''' Shows all records from the history table for the selected recurring expense.  Called from the Right Click -> Show Expense History menu option.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ShowExpenseHistoryToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ShowExpenseHistoryToolStripMenuItem1.Click
        ShowExpenseHistory()
    End Sub

#End Region

#Region "Recurring Income Right Click Menu Options"

    ''' <summary>
    ''' Add a new recurring income.  Called from the Right Click -> Add Income menu option.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AddIncomeToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles AddIncomeToolStripMenuItem1.Click
        AddRecurringIncome()
    End Sub

    ''' <summary>
    ''' Edit the selected recurring income.  Called from the Right Click -> Edit Income menu option.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub EditIncomeToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles EditIncomeToolStripMenuItem1.Click
        EditRecurringIncome(False)
    End Sub

    ''' <summary>
    ''' Delete the selected recurring income and any associated data.  Called from the Right Click -> Delete Income menu option.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub DeleteIncomeToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles DeleteIncomeToolStripMenuItem1.Click
        DeleteRecurringIncome()
    End Sub

    ''' <summary>
    ''' Show all records from the history table for the selected recurring income.  Called from the Right Click -> Show Income History menu option.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ShowIncomeHistoryToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ShowIncomeHistoryToolStripMenuItem1.Click
        ShowIncomeHistory()
    End Sub

#End Region

#Region "Notes Control Code"

    ''' <summary>
    ''' Save the current notes list when the control loses focus.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtNotes_LostFocus(sender As Object, e As EventArgs) Handles txtNotes.LostFocus
        SaveNotesData()
    End Sub

#End Region

#Region "UI Code"

    ''' <summary>
    ''' Updates the UI to display expenses for the specified month.
    ''' </summary>
    ''' <param name="StartMonth">Month to display expenses for.  This should be set to the first of the desired month.</param>
    Private Sub ShowExpensesFor(ByVal StartMonth As Date)

        ' Update the group box text to indicate the month and year that expenses are being displayed for.
        groupMonthExpenses.Text = String.Format("{0} {1} Income / Expenses", StartMonth.ToString("MMMM"), StartMonth.Year)

        ' Set the end date to the first of the following month.
        Dim EndMonth As Date = StartMonth.AddMonths(1)
        Me.ExpenseCalcuator.CalculateAllFutureExpenses(EndMonth)

        ' Generate a list of what expenses to display on the screen from the history and projections list.
        Dim ExpenseIds As Integer() = Me.ExpenseCalcuator.BudgetData.RecurringExpenses.Where(Function(e) e.ExpenseType = Options.ExpenseType.EXPENSE.ToString()
                                                                                      ).Select(Function(r) r.ExpenseId).ToArray()
        Dim MonthlyExpenses As New List(Of HistoryRecord)
        Dim SelectedExpenseHistory = Me.ExpenseCalcuator.BudgetData.ExpensesHistory.Where(Function(e) DateTime.Compare(e.ExpenseActualDate, StartMonth) >= 0 And
                                                                            DateTime.Compare(e.ExpenseActualDate, EndMonth) < 0 And
                                                                            ExpenseIds.Contains(e.ExpenseId)
                                                                            ).ToList()
        MonthlyExpenses.AddRange(SelectedExpenseHistory)
        Dim SelectedExpenseEstimates = Me.ExpenseCalcuator.ProjectedExpenses.Where(Function(e) DateTime.Compare(e.ExpenseActualDate, StartMonth) >= 0 And
                                                                                DateTime.Compare(e.ExpenseActualDate, EndMonth) < 0 And
                                                                                ExpenseIds.Contains(e.ExpenseId)
                                                                                ).ToList()
        MonthlyExpenses.AddRange(SelectedExpenseEstimates)

        ' Generate a list of what income to display on the screen from the history and projections list.
        Dim MonthlyIncome As New List(Of HistoryRecord)
        Dim SelectedIncomeHistory = Me.ExpenseCalcuator.BudgetData.ExpensesHistory.Where(Function(e) DateTime.Compare(e.ExpenseActualDate, StartMonth) >= 0 And
                                                                            DateTime.Compare(e.ExpenseActualDate, EndMonth) < 0 And
                                                                            Not ExpenseIds.Contains(e.ExpenseId)
                                                                            ).ToList()
        MonthlyIncome.AddRange(SelectedIncomeHistory)
        Dim SelectedIncomeEstimates = Me.ExpenseCalcuator.ProjectedExpenses.Where(Function(e) DateTime.Compare(e.ExpenseActualDate, StartMonth) >= 0 And
                                                                                DateTime.Compare(e.ExpenseActualDate, EndMonth) < 0 And
                                                                                Not ExpenseIds.Contains(e.ExpenseId)
                                                                                ).ToList()
        MonthlyIncome.AddRange(SelectedIncomeEstimates)

        ' Generate a list of past due expenses (anything before or on today that hasn't been marked as Paid).
        Dim PastDueExpenses = Me.ExpenseCalcuator.ProjectedExpenses.Where(Function(e) DateTime.Compare(e.ExpenseActualDate, DateTime.Now) < 0 And
                                                                                ExpenseIds.Contains(e.ExpenseId)
                                                                                ).ToList()

        ' Generate a list of past due income (anything before or on today that hasn't been marked as Received).
        Dim PastDueIncome = Me.ExpenseCalcuator.ProjectedExpenses.Where(Function(e) DateTime.Compare(e.ExpenseActualDate, DateTime.Now) < 0 And
                                                                                Not ExpenseIds.Contains(e.ExpenseId)
                                                                                ).ToList()

        ' Set the Estimated Expenses Amount for the month to the sum of the paid and projected expenses.
        Me.ToolStripStatusEstimatedAmount.Text = FormatCurrency(MonthlyExpenses.Sum(Function(e) e.ExpensePaidAmount))

        ' Set the Estimated Income Amount for the month to the sum of the received and projected income.
        Me.ToolStripStatusLabelEstimatedIncomeAmount.Text = FormatCurrency(MonthlyIncome.Sum(Function(e) e.ExpensePaidAmount))

        ' Set the Estimated Unpaid amount to the sum of the projected (unpaid) expenses.
        Me.ToolStripStatusEstimatedRemaining.Text = FormatCurrency(SelectedExpenseEstimates.Sum(Function(e) e.ExpensePaidAmount))

        ' Set the Total Paid amount to the sum of the paid expenses from the history table.
        Me.ToolStripStatusTotalPaid.Text = FormatCurrency(SelectedExpenseHistory.Sum(Function(e) e.ExpensePaidAmount))

        ' Set the Estimated Remaining income to the total projected income - the total projected expenses.
        Dim EstimatedUnusedIncome As Double = MonthlyIncome.Sum(Function(e) e.ExpensePaidAmount) - MonthlyExpenses.Sum(Function(e) e.ExpensePaidAmount)
        Me.ToolStripStatusLabelEstimatedRemainingIncomeAmount.Text = FormatCurrency(EstimatedUnusedIncome)
        If EstimatedUnusedIncome > 0 Then
            Me.ToolStripStatusLabelEstimatedRemainingIncomeAmount.ForeColor = Color.Green
        Else
            Me.ToolStripStatusLabelEstimatedRemainingIncomeAmount.ForeColor = Color.Red
        End If

        ' Reset the expenses grid
        Me.ListViewProjectedExpenses.Items.Clear()

        ' Load any monthly expenses into the expenses grid.
        LoadDataIntoExpensesGrid(MonthlyExpenses, Options.MonthlyGridGroups.MONTHLYEXPENSES)

        ' Load any past due expenses into the expenses gird.
        LoadDataIntoExpensesGrid(PastDueExpenses, Options.MonthlyGridGroups.PASTDUEEXPENSES)

        ' Load any monthly income into the grid.
        LoadDataIntoExpensesGrid(MonthlyIncome, Options.MonthlyGridGroups.MONTHLYINCOME)

        ' Load any past due income into the grid
        LoadDataIntoExpensesGrid(PastDueIncome, Options.MonthlyGridGroups.PASTDUEINCOME)

    End Sub

    ''' <summary>
    ''' Populates the list of expenses into the grid.
    ''' </summary>
    ''' <param name="ExpenseList">List of expenses to display in the grid</param>
    ''' <param name="GroupId">Indicates which group to load the expenses into (0 = Past Due, 1 = Monthly Expenses)</param>
    Private Sub LoadDataIntoExpensesGrid(ByRef ExpenseList As List(Of HistoryRecord), ByVal GroupId As Integer)

        For Each Expense As HistoryRecord In ExpenseList.OrderBy(Function(e) e.ExpenseActualDate).ToList()
            ' Create a new list item for each record in the ExpenseList list.
            Dim ExpenseItem As New ListViewItem
            ExpenseItem.Text = Expense.ExpenseActualDate.Value.ToShortDateString()
            ExpenseItem.SubItems.Add(Me.ExpenseCalcuator.BudgetData.RecurringExpenses.Where(Function(e) e.ExpenseId =
                                                                                                Expense.ExpenseId).FirstOrDefault().ExpenseDescription)
            ExpenseItem.SubItems.Add(FormatCurrency(Expense.ExpensePaidAmount))

            ' Set the tag to the ExpenseHistoryId to use with editing the record.
            ExpenseItem.Tag = Expense.ExpenseHistoryId

            ' Set the Group to display this record in based on GroupId.
            ExpenseItem.Group = ListViewProjectedExpenses.Groups(GroupId)

            ' Set the font based on if the expense is paid, unpaid or past due.
            If Expense.ExpenseHistoryId > 0 Then
                ' Show paid expenses as Green.
                ExpenseItem.Font = New Font(ListViewProjectedExpenses.Font, FontStyle.Regular)
                ExpenseItem.ForeColor = Color.Green

            Else
                If DateTime.Compare(Expense.ExpenseEstimatedDate, DateTime.Now) < 0 Then
                    ' Show past due expenses as Red
                    ExpenseItem.Font = New Font(ListViewProjectedExpenses.Font, FontStyle.Bold)
                    ExpenseItem.ForeColor = Color.Red
                End If
            End If

            ' Add this expense item to the list.
            Me.ListViewProjectedExpenses.Items.Add(ExpenseItem)
        Next

    End Sub

    ''' <summary>
    ''' This populates the recurring expenses list on the left of the UI.
    ''' </summary>
    Private Sub LoadExpenseList()

        ' Reset the Expenses list.  This allows this sub to be called any time the list needs to be re-drawn.
        treeRecurringExpenses.Nodes.Clear()

        ' Lopp through all of the available expense types and then load any recurring expenses configured for that type.
        For Each ExpenseType As ListOption In Me.ExpenseCalcuator.ExpenseRecurranceTypes
            ' Create a new entry for this type of expense.
            Dim NewCategory As New TreeNode
            NewCategory.Text = ExpenseType.DisplayName
            NewCategory.Tag = String.Format("Category|{0}", ExpenseType.Code) ' Setting the tag to distinguish between a category and an expense.

            ' Loop through each expense under this type.
            Dim SelectedExpenses = Me.ExpenseCalcuator.BudgetData.RecurringExpenses.Where(Function(f) f.ExpenseRecurranceTypeCode = ExpenseType.Code And
                                                                                   f.ExpenseType = Options.ExpenseType.EXPENSE.ToString()
                                                                                   ).OrderBy(Function(c) c.ExpenseNextDueDate).ToList()

            For Each Expense As ExpenseRecord In SelectedExpenses

                ' Create a new entry for this expense.
                Dim NewExpense As New TreeNode
                NewExpense.Text = String.Format("{0} {1} - {2}",
                                                If(Expense.ExpenseNextDueDate Is Nothing,
                                                    String.Empty,
                                                    Expense.ExpenseNextDueDate.Value.ToShortDateString()),
                                                Expense.ExpenseDescription,
                                                FormatCurrency(Expense.ExpenseAmount, 2)).TrimStart()
                NewExpense.Tag = String.Format("Expense|{0}", Expense.ExpenseId) ' Setting the tag to use for editing purposes.

                ' Add this expense to this expense type.
                NewCategory.Nodes.Add(NewExpense)
            Next

            ' Add this expense type to the screen.
            treeRecurringExpenses.Nodes.Add(NewCategory)
        Next

        ' This fixes the truncation issue with node display text
        treeRecurringExpenses.BackColor = Color.Empty

        ' Expand all the expense types by default
        treeRecurringExpenses.ExpandAll()

    End Sub

    ''' <summary>
    ''' This populates the recurring income list on the left of the UI.
    ''' </summary>
    Private Sub LoadIncomeList()

        ' Reset the Income list.  This allows this sub to be called any time the list needs to be re-drawn.
        treeRecurringIncome.Nodes.Clear()

        ' Lopp through all of the available types and then load any recurring income configured for that type.
        For Each IncomeType As ListOption In Me.ExpenseCalcuator.ExpenseRecurranceTypes
            ' Create a new entry for this type of income.
            Dim NewCategory As New TreeNode
            NewCategory.Text = IncomeType.DisplayName
            NewCategory.Tag = String.Format("Category|{0}", IncomeType.Code) ' Setting the tag to distinguish between a category and an income.

            ' Loop through each income under this type.
            Dim SelectedIncome = Me.ExpenseCalcuator.BudgetData.RecurringExpenses.Where(Function(f) f.ExpenseRecurranceTypeCode = IncomeType.Code And
                                                                                   f.ExpenseType = Options.ExpenseType.INCOME.ToString()
                                                                                   ).OrderBy(Function(c) c.ExpenseNextDueDate).ToList()

            For Each Income As ExpenseRecord In SelectedIncome

                ' Create a new entry for this income.
                Dim NewIncome As New TreeNode
                NewIncome.Text = String.Format("{0} {1} - {2}",
                                                If(Income.ExpenseNextDueDate Is Nothing,
                                                    String.Empty,
                                                    Income.ExpenseNextDueDate.Value.ToShortDateString()),
                                                Income.ExpenseDescription,
                                                FormatCurrency(Income.ExpenseAmount, 2)).TrimStart()
                NewIncome.Tag = String.Format("Income|{0}", Income.ExpenseId) ' Setting the tag to use for editing purposes.

                ' Add this income to this income type.
                NewCategory.Nodes.Add(NewIncome)
            Next

            ' Add this income type to the screen.
            treeRecurringIncome.Nodes.Add(NewCategory)
        Next

        ' This fixes the truncation issue with node display text
        treeRecurringIncome.BackColor = Color.Empty

        ' Expand all the income types by default
        treeRecurringIncome.ExpandAll()

    End Sub

    ''' <summary>
    ''' Creates the avaiable groups in the monthly expenses grid.
    ''' </summary>
    Private Sub LoadMonthlyExpenseGroups()

        ' Generate a list of available groups from the MonthlyGridGroups type
        Dim ExpenseGroups As List(Of ListOption) = Options.BuildList(GetType(Options.MonthlyGridGroups))

        ' Loop through the list and create a new group for each item and add to the ListViewProjectedExpenses control.
        For Each Group As ListOption In ExpenseGroups
            Dim NewGroup As New ListViewGroup
            NewGroup.Name = Group.Code
            NewGroup.Header = Group.DisplayName

            ListViewProjectedExpenses.Groups.Add(NewGroup)
        Next

    End Sub

#End Region

#Region "Child Window Code"

    ''' <summary>
    ''' Save a new record to the expense history table or update an existing item.  Called from the AddEditExpenseHistory dialog.
    ''' </summary>
    ''' <param name="ExpenseHistoryId">The ID of the history record.  
    ''' A positive number indicates this is an existing history record, a negative number indicates this is a calculated expense.</param>
    ''' <param name="AmountPaid">The actual amount paid for this history entry.</param>
    ''' <param name="DatePaid">The actual date paid for this history entry.</param>
    ''' <param name="ExpenseMemo">A short note or description about this specific payment.</param>
    Public Sub SaveExpenseHistory(ByVal ExpenseHistoryId As Integer, ByVal AmountPaid As Decimal, ByVal DatePaid As Date, ByVal ExpenseMemo As String)

        ' Call the expense calculator to update the existing history item or create a new one.
        Me.ExpenseCalcuator.SaveExpenseHistory(ExpenseHistoryId, AmountPaid, DatePaid, ExpenseMemo)

        ' Marking an entry as paid or updating an existing history entry can screw up existing calculations 
        '   so clear and re-load the display to reflect any changes.
        Me.LoadExpenseList()
        Me.LoadIncomeList()
        Me.ShowExpensesFor(DisplayMonth)

    End Sub

    ''' <summary>
    ''' Create a new recurring expense in the system.  Called from the Save button on the AddEditExpense form.
    ''' </summary>
    ''' <param name="ExpenseDetails">A new ExpenseRecord object with the configurations for the recurring expense.</param>
    Public Sub SaveExpense(ByVal ExpenseDetails As ExpenseRecord)

        If ExpenseDetails.ExpenseId = 0 Then
            ' If ExpenseId is 0 this this is a new record, add it to the database.
            Me.ExpenseCalcuator.AddNewExpense(ExpenseDetails)

        Else
            ' If expense id is not 0 then this is an existing record.  Update it in the database.
            Me.ExpenseCalcuator.UpdateExpense(ExpenseDetails)
        End If

        ' Re-load the recurring income and expenses lists to populate the new item into it.
        Me.LoadExpenseList()
        Me.LoadIncomeList()

        ' Refresh the grid to populate this new expense into it.
        ShowExpensesFor(DisplayMonth)

    End Sub

#End Region

#Region "Recurring Expense Management Code"

    ''' <summary>
    ''' Open the create new recurring expense window.
    ''' </summary>
    Private Sub AddRecurringExpense()

        ' Show the Add New Expense window.
        Dim NewExpenseWindow As New AddEditExpense(Me) ' Pass in this window so we can send data back.
        NewExpenseWindow.Tag = 0 ' Indicates new record.
        NewExpenseWindow.txtExpenseType.Text = Options.ExpenseType.EXPENSE.ToString() ' Indicates that this record is for an expense item
        NewExpenseWindow.Text = "Add Expense"
        NewExpenseWindow.ShowDialog()

    End Sub

    ''' <summary>
    ''' Edit the selected recurring expense.  Called from both the Edit Expense menu item and the list double click event.
    ''' </summary>
    ''' <param name="SurpressError">Indicates if the please select an expense error should be displayed.
    ''' Set to True to surpress the error when double clicking in the grid as a double click will collapse that category instead.</param>
    Private Sub EditRecurringExpense(ByVal SurpressError As Boolean)

        If treeRecurringExpenses.SelectedNode IsNot Nothing AndAlso treeRecurringExpenses.SelectedNode.Tag.ToString().StartsWith("Expense|") Then
            ' If the selected item is an expense, then extract it's id from the selected node's tag and load the configuration and history for that id.
            Dim ExpenseId As Integer = Convert.ToInt32(treeRecurringExpenses.SelectedNode.Tag.ToString().Split("|")(1))
            Dim ExpenseDetails As ExpenseRecord = Me.ExpenseCalcuator.BudgetData.RecurringExpenses.Where(Function(f) f.ExpenseId = ExpenseId).FirstOrDefault()

            Dim EditExpenseWindow As New AddEditExpense(Me) ' Pass in this window so we can send data back.
            EditExpenseWindow.LoadExpenseRecord(ExpenseDetails)
            EditExpenseWindow.Text = "Edit Expense"
            EditExpenseWindow.ShowDialog()

        Else
            ' If the selected item is not an expense, then display an error message.
            If SurpressError = False Then
                MessageBox.Show("Please select a Recurring Expense from the list.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End If

    End Sub

    ''' <summary>
    ''' Delete the selected recurring expense and any associated data.
    ''' </summary>
    Private Sub DeleteRecurringExpense()

        ' Make sure the selected record belongs to an expense and not a group title.
        If treeRecurringExpenses.SelectedNode IsNot Nothing AndAlso treeRecurringExpenses.SelectedNode.Tag.ToString().StartsWith("Expense|") Then

            ' Locate the expense to be deleted and generate a confirmation message.
            Dim ExpenseId As Integer = Convert.ToInt32(treeRecurringExpenses.SelectedNode.Tag.ToString().Split("|")(1))
            Dim ExpenseDetails As ExpenseRecord = Me.ExpenseCalcuator.BudgetData.RecurringExpenses.Where(Function(f) f.ExpenseId = ExpenseId).FirstOrDefault()
            Dim Message As String = String.Format("Warning:  You are about to delete the ""{0}"" Recurring Expense and all associated payment records.  Continue?",
                                                  ExpenseDetails.ExpenseDescription)

            If MessageBox.Show(Message, "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.Yes Then
                ' If the user presses Yes, then delete the expense.
                Me.ExpenseCalcuator.DeleteRecurringExpense(ExpenseId)

                ' Re-load the recurring expenses list to remove the deleted expense from the list.
                Me.LoadExpenseList()

                ' Refresh the grid to remove any paid and projected expenses that were tied to the record that was deleted.
                Me.ShowExpensesFor(DisplayMonth)

            End If
        Else
            MessageBox.Show("Please select a Recurring Expense from the list.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    ''' <summary>
    ''' Opens a new window to show the payment history for the selected expense.
    ''' </summary>
    Private Sub ShowExpenseHistory()

        ' Check and see which item is selected.
        If treeRecurringExpenses.SelectedNode IsNot Nothing AndAlso treeRecurringExpenses.SelectedNode.Tag.ToString().StartsWith("Expense|") Then

            ' If the selected item is an expense, then extract it's id from the selected node's tag and load the configuration and history for that id.
            Dim ExpenseId As Integer = Convert.ToInt32(treeRecurringExpenses.SelectedNode.Tag.ToString().Split("|")(1))
            Dim ExpenseHistoryRecords As List(Of HistoryRecord) = Me.ExpenseCalcuator.BudgetData.ExpensesHistory.Where(Function(f) f.ExpenseId = ExpenseId).ToList()
            Dim ExpenseDetails As ExpenseRecord = Me.ExpenseCalcuator.BudgetData.RecurringExpenses.Where(Function(f) f.ExpenseId = ExpenseId).FirstOrDefault()

            ' Pass the loaded data into a new history window and display it.
            Dim HistoryWindow As New ViewExpenseHistory(ExpenseHistoryRecords, ExpenseDetails)
            HistoryWindow.Show()

        Else
            ' If the selected item is not an expense, then display an error message.
            MessageBox.Show("Please select a Recurring Expense from the list.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

#End Region

#Region "Recurring Income Management Code"

    ''' <summary>
    ''' Open the create new recurring income window.
    ''' </summary>
    Private Sub AddRecurringIncome()

        ' Show the Add New Expense window.
        Dim NewExpenseWindow As New AddEditExpense(Me) ' Pass in this window so we can send data back.
        NewExpenseWindow.Tag = 0 ' Indicates new record.
        NewExpenseWindow.txtExpenseType.Text = Options.ExpenseType.INCOME.ToString() ' Indicates that this record is for an expense item
        NewExpenseWindow.Text = "Add Income"
        NewExpenseWindow.ShowDialog()

    End Sub

    ''' <summary>
    ''' Edit the selected recurring income.  Called from both the Edit Income menu item and the list double click event.
    ''' </summary>
    ''' <param name="SurpressError">Indicates if the please select an income error should be displayed.
    ''' Set to True to surpress the error when double clicking in the grid as a double click will collapse that category instead.</param>
    Private Sub EditRecurringIncome(ByVal SurpressError As Boolean)

        If treeRecurringIncome.SelectedNode IsNot Nothing AndAlso treeRecurringIncome.SelectedNode.Tag.ToString().StartsWith("Income|") Then
            ' If the selected item is an income, then extract it's id from the selected node's tag and load the configuration and history for that id.
            Dim IncomeId As Integer = Convert.ToInt32(treeRecurringIncome.SelectedNode.Tag.ToString().Split("|")(1))
            Dim IncomeDetails As ExpenseRecord = Me.ExpenseCalcuator.BudgetData.RecurringExpenses.Where(Function(f) f.ExpenseId = IncomeId).FirstOrDefault()

            Dim EditExpenseWindow As New AddEditExpense(Me) ' Pass in this window so we can send data back.
            EditExpenseWindow.LoadExpenseRecord(IncomeDetails)
            EditExpenseWindow.Text = "Edit Income"
            EditExpenseWindow.ShowDialog()

        Else
            ' If the selected item is not an income, then display an error message.
            If SurpressError = False Then
                MessageBox.Show("Please select a Recurring Income from the list.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End If

    End Sub

    ''' <summary>
    ''' Delete the selected recurring income and any associated data.
    ''' </summary>
    Private Sub DeleteRecurringIncome()

        ' Make sure the selected record belongs to an income and not a group title.
        If treeRecurringIncome.SelectedNode IsNot Nothing AndAlso treeRecurringIncome.SelectedNode.Tag.ToString().StartsWith("Income|") Then

            ' Locate the expense to be deleted and generate a confirmation message.
            Dim IncomeId As Integer = Convert.ToInt32(treeRecurringIncome.SelectedNode.Tag.ToString().Split("|")(1))
            Dim IncomeDetails As ExpenseRecord = Me.ExpenseCalcuator.BudgetData.RecurringExpenses.Where(Function(f) f.ExpenseId = IncomeId).FirstOrDefault()
            Dim Message As String = String.Format("Warning:  You are about to delete the ""{0}"" Recurring Income and all associated payment records.  Continue?",
                                                  IncomeDetails.ExpenseDescription)

            If MessageBox.Show(Message, "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.Yes Then
                ' If the user presses Yes, then delete the expense.
                Me.ExpenseCalcuator.DeleteRecurringExpense(IncomeId)

                ' Re-load the recurring expenses list to remove the deleted expense from the list.
                Me.LoadIncomeList()

                ' Refresh the grid to remove any paid and projected expenses that were tied to the record that was deleted.
                Me.ShowExpensesFor(DisplayMonth)

            End If
        Else
            MessageBox.Show("Please select a Recurring Income from the list.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    ''' <summary>
    ''' Opens a new window to show the income history for the selected income.
    ''' </summary>
    Private Sub ShowIncomeHistory()

        ' Check and see which item is selected.
        If treeRecurringIncome.SelectedNode IsNot Nothing AndAlso treeRecurringIncome.SelectedNode.Tag.ToString().StartsWith("Income|") Then

            ' If the selected item is an income, then extract it's id from the selected node's tag and load the configuration and history for that id.
            Dim IncomeId As Integer = Convert.ToInt32(treeRecurringIncome.SelectedNode.Tag.ToString().Split("|")(1))
            Dim IncomeHistoryRecords As List(Of HistoryRecord) = Me.ExpenseCalcuator.BudgetData.ExpensesHistory.Where(Function(f) f.ExpenseId = IncomeId).ToList()
            Dim IncomeDetails As ExpenseRecord = Me.ExpenseCalcuator.BudgetData.RecurringExpenses.Where(Function(f) f.ExpenseId = IncomeId).FirstOrDefault()

            ' Pass the loaded data into a new history window and display it.
            Dim HistoryWindow As New ViewExpenseHistory(IncomeHistoryRecords, IncomeDetails)
            HistoryWindow.Show()

        Else
            ' If the selected item is not an income, then display an error message.
            MessageBox.Show("Please select a Recurring Income from the list.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

#End Region

#Region "Notes Code"

    ''' <summary>
    ''' Save the current notes text to disk.
    ''' </summary>
    Private Sub SaveNotesData()

        ' Read the current text from the screen.
        Me.ExpenseCalcuator.BudgetData.Notes = txtNotes.Text

        ' Save the data.
        Me.ExpenseCalcuator.BudgetData.SaveData()

    End Sub

#End Region

End Class
