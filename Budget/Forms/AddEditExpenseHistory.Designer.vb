﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddEditExpenseHistory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddEditExpenseHistory))
        Me.DateTimePaid = New System.Windows.Forms.DateTimePicker()
        Me.lblDatePaid = New System.Windows.Forms.Label()
        Me.txtAmount = New System.Windows.Forms.TextBox()
        Me.lblAmount = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.buttonSubtractOneDay = New System.Windows.Forms.Button()
        Me.buttonAddOneDay = New System.Windows.Forms.Button()
        Me.lblExpenseMemo = New System.Windows.Forms.Label()
        Me.txtExpenseMemo = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'DateTimePaid
        '
        Me.DateTimePaid.Location = New System.Drawing.Point(110, 9)
        Me.DateTimePaid.Name = "DateTimePaid"
        Me.DateTimePaid.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePaid.TabIndex = 12
        '
        'lblDatePaid
        '
        Me.lblDatePaid.AutoSize = True
        Me.lblDatePaid.Location = New System.Drawing.Point(12, 15)
        Me.lblDatePaid.Name = "lblDatePaid"
        Me.lblDatePaid.Size = New System.Drawing.Size(57, 13)
        Me.lblDatePaid.TabIndex = 11
        Me.lblDatePaid.Text = "Date Paid:"
        '
        'txtAmount
        '
        Me.txtAmount.Location = New System.Drawing.Point(110, 36)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(100, 20)
        Me.txtAmount.TabIndex = 14
        '
        'lblAmount
        '
        Me.lblAmount.AutoSize = True
        Me.lblAmount.Location = New System.Drawing.Point(12, 39)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(70, 13)
        Me.lblAmount.TabIndex = 13
        Me.lblAmount.Text = "Amount Paid:"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(286, 149)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 19
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(200, 149)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 18
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'buttonSubtractOneDay
        '
        Me.buttonSubtractOneDay.Location = New System.Drawing.Point(316, 8)
        Me.buttonSubtractOneDay.Name = "buttonSubtractOneDay"
        Me.buttonSubtractOneDay.Size = New System.Drawing.Size(23, 23)
        Me.buttonSubtractOneDay.TabIndex = 20
        Me.buttonSubtractOneDay.Text = "-"
        Me.buttonSubtractOneDay.UseVisualStyleBackColor = True
        '
        'buttonAddOneDay
        '
        Me.buttonAddOneDay.Location = New System.Drawing.Point(345, 8)
        Me.buttonAddOneDay.Name = "buttonAddOneDay"
        Me.buttonAddOneDay.Size = New System.Drawing.Size(23, 23)
        Me.buttonAddOneDay.TabIndex = 21
        Me.buttonAddOneDay.Text = "+"
        Me.buttonAddOneDay.UseVisualStyleBackColor = True
        '
        'lblExpenseMemo
        '
        Me.lblExpenseMemo.AutoSize = True
        Me.lblExpenseMemo.Location = New System.Drawing.Point(12, 63)
        Me.lblExpenseMemo.Name = "lblExpenseMemo"
        Me.lblExpenseMemo.Size = New System.Drawing.Size(39, 13)
        Me.lblExpenseMemo.TabIndex = 22
        Me.lblExpenseMemo.Text = "Memo:"
        '
        'txtExpenseMemo
        '
        Me.txtExpenseMemo.Location = New System.Drawing.Point(110, 63)
        Me.txtExpenseMemo.Multiline = True
        Me.txtExpenseMemo.Name = "txtExpenseMemo"
        Me.txtExpenseMemo.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.txtExpenseMemo.Size = New System.Drawing.Size(258, 80)
        Me.txtExpenseMemo.TabIndex = 23
        '
        'AddEditExpenseHistory
        '
        Me.AcceptButton = Me.btnSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(373, 181)
        Me.Controls.Add(Me.txtExpenseMemo)
        Me.Controls.Add(Me.lblExpenseMemo)
        Me.Controls.Add(Me.buttonAddOneDay)
        Me.Controls.Add(Me.buttonSubtractOneDay)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.txtAmount)
        Me.Controls.Add(Me.lblAmount)
        Me.Controls.Add(Me.DateTimePaid)
        Me.Controls.Add(Me.lblDatePaid)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AddEditExpenseHistory"
        Me.Text = "Edit Expense Details"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DateTimePaid As DateTimePicker
    Friend WithEvents lblDatePaid As Label
    Friend WithEvents txtAmount As TextBox
    Friend WithEvents lblAmount As Label
    Friend WithEvents btnSave As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents buttonSubtractOneDay As Button
    Friend WithEvents buttonAddOneDay As Button
    Friend WithEvents lblExpenseMemo As Label
    Friend WithEvents txtExpenseMemo As TextBox
End Class
