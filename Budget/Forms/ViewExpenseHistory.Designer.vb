﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ViewExpenseHistory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ViewExpenseHistory))
        Me.ListViewPaidExpenses = New System.Windows.Forms.ListView()
        Me.ColumnDueDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnPaidDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnDescription = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnMemo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnAmount = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnPaidAmount = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SuspendLayout()
        '
        'ListViewPaidExpenses
        '
        Me.ListViewPaidExpenses.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnDueDate, Me.ColumnPaidDate, Me.ColumnDescription, Me.ColumnMemo, Me.ColumnAmount, Me.ColumnPaidAmount})
        Me.ListViewPaidExpenses.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListViewPaidExpenses.FullRowSelect = True
        Me.ListViewPaidExpenses.HideSelection = False
        Me.ListViewPaidExpenses.Location = New System.Drawing.Point(0, 0)
        Me.ListViewPaidExpenses.Name = "ListViewPaidExpenses"
        Me.ListViewPaidExpenses.Size = New System.Drawing.Size(784, 561)
        Me.ListViewPaidExpenses.TabIndex = 6
        Me.ListViewPaidExpenses.UseCompatibleStateImageBehavior = False
        Me.ListViewPaidExpenses.View = System.Windows.Forms.View.Details
        '
        'ColumnDueDate
        '
        Me.ColumnDueDate.Text = "Estimated Date"
        Me.ColumnDueDate.Width = 100
        '
        'ColumnPaidDate
        '
        Me.ColumnPaidDate.Text = "Actual Date"
        Me.ColumnPaidDate.Width = 100
        '
        'ColumnDescription
        '
        Me.ColumnDescription.Text = "Description"
        Me.ColumnDescription.Width = 190
        '
        'ColumnMemo
        '
        Me.ColumnMemo.Text = "Memo"
        Me.ColumnMemo.Width = 170
        '
        'ColumnAmount
        '
        Me.ColumnAmount.Text = "Estimated Amount"
        Me.ColumnAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnAmount.Width = 100
        '
        'ColumnPaidAmount
        '
        Me.ColumnPaidAmount.Text = "Actual Amount"
        Me.ColumnPaidAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnPaidAmount.Width = 100
        '
        'ViewExpenseHistory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 561)
        Me.Controls.Add(Me.ListViewPaidExpenses)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ViewExpenseHistory"
        Me.Text = "Expense History For"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ListViewPaidExpenses As ListView
    Friend WithEvents ColumnDueDate As ColumnHeader
    Friend WithEvents ColumnDescription As ColumnHeader
    Friend WithEvents ColumnAmount As ColumnHeader
    Friend WithEvents ColumnPaidDate As ColumnHeader
    Friend WithEvents ColumnMemo As ColumnHeader
    Friend WithEvents ColumnPaidAmount As ColumnHeader
End Class
