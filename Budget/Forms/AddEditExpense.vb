﻿' Project Name:  Budget
' Copyright 2018 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 23 June 2018

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class AddEditExpense

#Region "Properties"

    Public ParentWindow As BudgetMain ' Used to pass data back to the main window.

#End Region

#Region "Main Form Control Code"

    ''' <summary>
    ''' Create a new instance of the Add / Edit Expense window.
    ''' </summary>
    ''' <param name="ParentWindow">A reference to the parent window to use to send the new / modified expense details back.</param>
    Sub New(ParentWindow)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        ' Disable the End Date field to match the checkbox.
        Me.ParentWindow = ParentWindow
        DateTimeEndDate.Enabled = False

        ' Populate the combo boxes with the data from the main window.
        For Each RecurranceType As ListOption In Me.ParentWindow.ExpenseCalcuator.ExpenseRecurranceTypes
            comboRecurranceType.Items.Add(RecurranceType.DisplayName)
        Next

        For Each NextType As ListOption In Me.ParentWindow.ExpenseCalcuator.ExpenseCalculationTypes
            comboNextRecurranceType.Items.Add(NextType.DisplayName)
        Next
    End Sub

#End Region

#Region "Window Controls Code"

    ''' <summary>
    ''' Fires when the End Date check box is checked / unchecked to update corresponding screen elements.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub checkEndDate_CheckedChanged(sender As Object, e As EventArgs) Handles checkEndDate.CheckedChanged
        ' If the check box is checked, the end date field is enabled, otherwise the end date field is disabled.
        '   The date control for the end date field is always populated with a date, so this allows us to know if we
        '   should use that date when creating the expense record or ignore it.
        DateTimeEndDate.Enabled = (checkEndDate.Checked = True)
    End Sub

    ''' <summary>
    ''' Fires when the cancel button is clicked to close the window and cancel any edits.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        ' Close this window.
        Me.Close()
    End Sub

    ''' <summary>
    ''' Fires when the Save button is clicked to validate the form and save the expense record.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        ' Confirm that the form is valid before saving the screen.
        If ValidateForm() = True Then

            ' The configured expense information is valid.  Populate a new ExpenseRecord object and send it back to the main window to be saved.
            Dim Expense As New ExpenseRecord
            With Expense
                .ExpenseAmount = txtAmount.Text
                .ExpenseStartDate = DateTimeStartDate.Text
                .ExpenseRecurranceTypeCode = Me.ParentWindow.ExpenseCalcuator.ExpenseRecurranceTypes.Where(Function(f) f.DisplayName = comboRecurranceType.SelectedItem).First.Code
                .ExpenseRecurranceInterval = txtRecurranceInterval.Text
                .ExpenseNextRecurranceTypeCode = Me.ParentWindow.ExpenseCalcuator.ExpenseCalculationTypes.Where(Function(f) f.DisplayName = comboNextRecurranceType.SelectedItem).First.Code
                If checkEndDate.Checked Then
                    .ExpenseEndDate = New Date(DateTimeEndDate.Value.Year, DateTimeEndDate.Value.Month, DateTimeEndDate.Value.Day)
                End If
                .ExpenseDescription = txtDescription.Text
                .ExpenseType = txtExpenseType.Text

                ' Set the expense id to the tag.  This will be 0 for a new record, or a valid id if editing an existing record.
                .ExpenseId = Me.Tag
            End With

            ' Save this recurring expense
            Me.ParentWindow.SaveExpense(Expense)

            ' Close this window.
            Me.Close()

        End If
    End Sub

    ''' <summary>
    ''' Fires when the Recurrance Type combo box selection is changed to update any corresponding screen elements.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub comboRecurranceType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles comboRecurranceType.SelectedIndexChanged
        ' If expense type is last day of the month, then set recurrance to once a month and disable the fields
        '   as this is the only configuration that the calculation code can handle for last day of the month.
        ' Otherwise enable the recurrance fields.
        If comboRecurranceType.SelectedItem.ToString =
            Options.GetDescription(GetType(Options.RecurranceType), Options.RecurranceType.LASTDAYOFMONTH.ToString()) Then
            comboNextRecurranceType.SelectedIndex = 0
            comboNextRecurranceType.Enabled = False
            txtRecurranceInterval.Text = 1
            txtRecurranceInterval.Enabled = False

        Else
            comboNextRecurranceType.Enabled = True
            txtRecurranceInterval.Enabled = True
        End If
    End Sub

#End Region

#Region "Public Functions / Subs"

    ''' <summary>
    ''' Loads an existing expense record onto the screen to be edited.
    ''' </summary>
    ''' <param name="ExpenseDetails">The details of the expense record to edit.</param>
    Public Sub LoadExpenseRecord(ByRef ExpenseDetails As ExpenseRecord)
        With ExpenseDetails
            Me.Tag = .ExpenseId ' Used by the save logic to determine which record is loaded.

            ' Populate the text and date fields.
            txtAmount.Text = .ExpenseAmount
            txtDescription.Text = .ExpenseDescription
            txtRecurranceInterval.Text = .ExpenseRecurranceInterval
            DateTimeStartDate.Value = .ExpenseStartDate
            txtExpenseType.Text = .ExpenseType

            ' Only populate End Date if it's set, and also update the UI if populating it.
            If .ExpenseEndDate IsNot Nothing Then
                DateTimeEndDate.Value = .ExpenseEndDate
                checkEndDate.Checked = True
                DateTimeEndDate.Enabled = True
            End If

            ' Select the correct value in the "Calculate next due date from:" combo box.
            Dim RecurranceTypeName As String = Me.ParentWindow.ExpenseCalcuator.ExpenseCalculationTypes.Where(Function(r) r.Code = .ExpenseNextRecurranceTypeCode).First.DisplayName
            For Each item In comboNextRecurranceType.Items
                If item.ToString() = RecurranceTypeName Then
                    comboNextRecurranceType.SelectedItem = item
                End If
            Next

            ' Select the correct value in the "Recurrance Type:" combo box.
            Dim ExpenseTypeName As String = Me.ParentWindow.ExpenseCalcuator.ExpenseRecurranceTypes.Where(Function(e) e.Code = .ExpenseRecurranceTypeCode).First.DisplayName
            For Each item In comboRecurranceType.Items
                If item.ToString() = ExpenseTypeName Then
                    comboRecurranceType.SelectedItem = item
                End If
            Next

            ' Disable everything except for description, end date and amount.
            txtRecurranceInterval.Enabled = False
            DateTimeStartDate.Enabled = False
            comboNextRecurranceType.Enabled = False
            comboRecurranceType.Enabled = False

        End With
    End Sub

#End Region

#Region "Private Functions / Subs"

    ''' <summary>
    ''' Validate that all required fields are populated and that all fields have data of the correct type.
    ''' </summary>
    ''' <returns>Returns True if the form is valid, otherwise returns false.</returns>
    Private Function ValidateForm() As Boolean

        ' Confirm all of the fields are populated.
        If txtAmount.Text.Trim = String.Empty OrElse
                comboRecurranceType.SelectedIndex < 0 OrElse
                txtRecurranceInterval.Text.Trim = String.Empty OrElse
                comboNextRecurranceType.SelectedIndex < 0 OrElse
                txtDescription.Text.Trim = String.Empty Then
            MessageBox.Show("Please fill in all fields.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If

        ' Confirm the number fields contain numbers and not text.
        If Not IsNumeric(txtAmount.Text) Then
            MessageBox.Show("Amount must be a number.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If

        If Not IsNumeric(txtRecurranceInterval.Text) Then
            MessageBox.Show("Interval must be a number.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If

        ' If recurrance is set to last day of the month, then confirm that starting date is the last day of the selected month,
        '    or else the expense will start on on whatever day is selected and then switch to last day of the month going forward.
        If comboRecurranceType.SelectedItem.ToString =
            Options.GetDescription(GetType(Options.RecurranceType), Options.RecurranceType.LASTDAYOFMONTH.ToString()) Then
            Dim TempDate As New Date(DateTimeStartDate.Value.Year, DateTimeStartDate.Value.Month, 1)
            TempDate = TempDate.AddMonths(1)
            TempDate = TempDate.AddDays(-1)
            If DateTimeStartDate.Value.Day <> TempDate.Day Then
                MessageBox.Show("Start Date must be the last day of the month.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End If
        End If

        ' Return true to indicate that the form is valid.
        Return True

    End Function

#End Region

End Class