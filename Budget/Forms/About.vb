﻿' Project Name:  Budget
' Copyright 2018 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 23 June 2018

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class formAbout

#Region "Main Form Control Code"

    ''' <summary>
    ''' Load the About window.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub formAbout_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Set the version and copyright info when the window loads.
        Me.lblVersion.Text = Me.lblVersion.Text & My.Application.Info.Version.ToString
        Me.lblCopyright.Text = Me.lblCopyright.Text & My.Application.Info.Copyright
    End Sub

    ''' <summary>
    ''' Close the About window.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub buttonOK_Click(sender As Object, e As EventArgs) Handles buttonOK.Click
        ' Close the window
        Me.Close()
    End Sub

#End Region

End Class