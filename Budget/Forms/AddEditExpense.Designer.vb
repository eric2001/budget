﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddEditExpense
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddEditExpense))
        Me.lblAmount = New System.Windows.Forms.Label()
        Me.txtAmount = New System.Windows.Forms.TextBox()
        Me.lblDueDate = New System.Windows.Forms.Label()
        Me.lblRecurranceType = New System.Windows.Forms.Label()
        Me.comboRecurranceType = New System.Windows.Forms.ComboBox()
        Me.lblInterval = New System.Windows.Forms.Label()
        Me.txtRecurranceInterval = New System.Windows.Forms.TextBox()
        Me.lblNextDueDate = New System.Windows.Forms.Label()
        Me.comboNextRecurranceType = New System.Windows.Forms.ComboBox()
        Me.DateTimeStartDate = New System.Windows.Forms.DateTimePicker()
        Me.DateTimeEndDate = New System.Windows.Forms.DateTimePicker()
        Me.checkEndDate = New System.Windows.Forms.CheckBox()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.txtExpenseType = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'lblAmount
        '
        Me.lblAmount.AutoSize = True
        Me.lblAmount.Location = New System.Drawing.Point(13, 13)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(46, 13)
        Me.lblAmount.TabIndex = 0
        Me.lblAmount.Text = "Amount:"
        '
        'txtAmount
        '
        Me.txtAmount.Location = New System.Drawing.Point(165, 9)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(100, 20)
        Me.txtAmount.TabIndex = 1
        '
        'lblDueDate
        '
        Me.lblDueDate.AutoSize = True
        Me.lblDueDate.Location = New System.Drawing.Point(13, 37)
        Me.lblDueDate.Name = "lblDueDate"
        Me.lblDueDate.Size = New System.Drawing.Size(78, 13)
        Me.lblDueDate.TabIndex = 2
        Me.lblDueDate.Text = "First Due Date:"
        '
        'lblRecurranceType
        '
        Me.lblRecurranceType.AutoSize = True
        Me.lblRecurranceType.Location = New System.Drawing.Point(13, 61)
        Me.lblRecurranceType.Name = "lblRecurranceType"
        Me.lblRecurranceType.Size = New System.Drawing.Size(93, 13)
        Me.lblRecurranceType.TabIndex = 4
        Me.lblRecurranceType.Text = "Recurrance Type:"
        '
        'comboRecurranceType
        '
        Me.comboRecurranceType.FormattingEnabled = True
        Me.comboRecurranceType.Location = New System.Drawing.Point(165, 57)
        Me.comboRecurranceType.Name = "comboRecurranceType"
        Me.comboRecurranceType.Size = New System.Drawing.Size(121, 21)
        Me.comboRecurranceType.TabIndex = 5
        '
        'lblInterval
        '
        Me.lblInterval.AutoSize = True
        Me.lblInterval.Location = New System.Drawing.Point(13, 85)
        Me.lblInterval.Name = "lblInterval"
        Me.lblInterval.Size = New System.Drawing.Size(45, 13)
        Me.lblInterval.TabIndex = 6
        Me.lblInterval.Text = "Interval:"
        '
        'txtRecurranceInterval
        '
        Me.txtRecurranceInterval.Location = New System.Drawing.Point(165, 81)
        Me.txtRecurranceInterval.Name = "txtRecurranceInterval"
        Me.txtRecurranceInterval.Size = New System.Drawing.Size(100, 20)
        Me.txtRecurranceInterval.TabIndex = 7
        '
        'lblNextDueDate
        '
        Me.lblNextDueDate.AutoSize = True
        Me.lblNextDueDate.Location = New System.Drawing.Point(13, 109)
        Me.lblNextDueDate.Name = "lblNextDueDate"
        Me.lblNextDueDate.Size = New System.Drawing.Size(145, 13)
        Me.lblNextDueDate.TabIndex = 8
        Me.lblNextDueDate.Text = "Calculate next due date from:"
        '
        'comboNextRecurranceType
        '
        Me.comboNextRecurranceType.FormattingEnabled = True
        Me.comboNextRecurranceType.Location = New System.Drawing.Point(165, 105)
        Me.comboNextRecurranceType.Name = "comboNextRecurranceType"
        Me.comboNextRecurranceType.Size = New System.Drawing.Size(121, 21)
        Me.comboNextRecurranceType.TabIndex = 9
        '
        'DateTimeStartDate
        '
        Me.DateTimeStartDate.Location = New System.Drawing.Point(165, 33)
        Me.DateTimeStartDate.Name = "DateTimeStartDate"
        Me.DateTimeStartDate.Size = New System.Drawing.Size(200, 20)
        Me.DateTimeStartDate.TabIndex = 10
        '
        'DateTimeEndDate
        '
        Me.DateTimeEndDate.Location = New System.Drawing.Point(165, 131)
        Me.DateTimeEndDate.Name = "DateTimeEndDate"
        Me.DateTimeEndDate.Size = New System.Drawing.Size(200, 20)
        Me.DateTimeEndDate.TabIndex = 12
        '
        'checkEndDate
        '
        Me.checkEndDate.AutoSize = True
        Me.checkEndDate.Location = New System.Drawing.Point(13, 133)
        Me.checkEndDate.Name = "checkEndDate"
        Me.checkEndDate.Size = New System.Drawing.Size(74, 17)
        Me.checkEndDate.TabIndex = 13
        Me.checkEndDate.Text = "End Date:"
        Me.checkEndDate.UseVisualStyleBackColor = True
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.Location = New System.Drawing.Point(13, 161)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(63, 13)
        Me.lblDescription.TabIndex = 14
        Me.lblDescription.Text = "Description:"
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(165, 157)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.txtDescription.Size = New System.Drawing.Size(200, 92)
        Me.txtDescription.TabIndex = 15
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(211, 255)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 16
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(297, 255)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 17
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'txtExpenseType
        '
        Me.txtExpenseType.Location = New System.Drawing.Point(30, 241)
        Me.txtExpenseType.Name = "txtExpenseType"
        Me.txtExpenseType.Size = New System.Drawing.Size(100, 20)
        Me.txtExpenseType.TabIndex = 18
        Me.txtExpenseType.Visible = False
        '
        'AddEditExpense
        '
        Me.AcceptButton = Me.btnSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(384, 288)
        Me.Controls.Add(Me.txtExpenseType)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.checkEndDate)
        Me.Controls.Add(Me.DateTimeEndDate)
        Me.Controls.Add(Me.DateTimeStartDate)
        Me.Controls.Add(Me.comboNextRecurranceType)
        Me.Controls.Add(Me.lblNextDueDate)
        Me.Controls.Add(Me.txtRecurranceInterval)
        Me.Controls.Add(Me.lblInterval)
        Me.Controls.Add(Me.comboRecurranceType)
        Me.Controls.Add(Me.lblRecurranceType)
        Me.Controls.Add(Me.lblDueDate)
        Me.Controls.Add(Me.txtAmount)
        Me.Controls.Add(Me.lblAmount)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AddEditExpense"
        Me.Text = "Add Expense"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblAmount As Label
    Friend WithEvents txtAmount As TextBox
    Friend WithEvents lblDueDate As Label
    Friend WithEvents lblRecurranceType As Label
    Friend WithEvents comboRecurranceType As ComboBox
    Friend WithEvents lblInterval As Label
    Friend WithEvents txtRecurranceInterval As TextBox
    Friend WithEvents lblNextDueDate As Label
    Friend WithEvents comboNextRecurranceType As ComboBox
    Friend WithEvents DateTimeStartDate As DateTimePicker
    Friend WithEvents DateTimeEndDate As DateTimePicker
    Friend WithEvents checkEndDate As CheckBox
    Friend WithEvents lblDescription As Label
    Friend WithEvents txtDescription As TextBox
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents txtExpenseType As TextBox
End Class
