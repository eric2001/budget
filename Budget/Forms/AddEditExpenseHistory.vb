﻿' Project Name:  Budget
' Copyright 2018 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 23 June 2018

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class AddEditExpenseHistory

#Region "Properties"

    Public ParentWindow As BudgetMain ' Used to pass data back to the main window.

#End Region

#Region "Window Controls Code"

    ''' <summary>
    ''' Fires when the Cancel button is clicked to cancel the edit and close the window.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click

        ' Close this window.
        Me.Close()

    End Sub

    ''' <summary>
    ''' Fires when the save button is clicked to validate the form and close the window.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        ' Confirm that the form is valid before saving the history record
        If Me.ValidateForm() = True Then

            ' Pass the specified values up to the parent window to either update an existing history record or create a new one.
            Me.ParentWindow.SaveExpenseHistory(Me.Tag, txtAmount.Text, DateTimePaid.Value, txtExpenseMemo.Text)

            ' Close this window.
            Me.Close()

        End If
    End Sub

    ''' <summary>
    ''' Fires when the minus button is clicked to update the selected date.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub buttonSubtractOneDay_Click(sender As Object, e As EventArgs) Handles buttonSubtractOneDay.Click

        ' Subtract one day from the selected Date.
        '   Date Paid defaults to estimated date so this makes it easier to quickly change the date by a day or two if needed.
        DateTimePaid.Value = DateTimePaid.Value.AddDays(-1)

    End Sub

    ''' <summary>
    ''' Fires when the plus button is clicked to update the selected date.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub buttonAddOneDay_Click(sender As Object, e As EventArgs) Handles buttonAddOneDay.Click

        ' Add one day to the selected Date.
        '   Date Paid defaults to estimated date so this makes it easier to quickly change the date by a day or two if needed.
        DateTimePaid.Value = DateTimePaid.Value.AddDays(1)

    End Sub

#End Region

#Region "Private Functions / Subs"

    ''' <summary>
    ''' Confirms that all required fieds are filled in and have data of a compatible type.
    ''' </summary>
    ''' <returns>Returns True if the form is valid, otherwise returns false.</returns>
    Private Function ValidateForm() As Boolean

        ' Confirm that an amount was entered and that it is a number.
        If txtAmount.Text = String.Empty OrElse Not IsNumeric(txtAmount.Text) Then
            MessageBox.Show("Please enter an amount.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If

        ' Return true to indicate that the form is valid
        Return True

    End Function

#End Region

End Class