﻿' Project Name:  Budget
' Copyright 2018 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 23 June 2018

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class ViewExpenseHistory

#Region "Main Form Control Code"

    ''' <summary>
    ''' Load the history records to be displayed into the window.
    ''' </summary>
    ''' <param name="HistoryRecords"></param>
    ''' <param name="ExpenseDetails"></param>
    Public Sub New(ByRef HistoryRecords As List(Of HistoryRecord), ByRef ExpenseDetails As ExpenseRecord)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        ' Update the window title to indicate which expense we're displaying history data for.
        If ExpenseDetails.ExpenseType = Options.ExpenseType.EXPENSE.ToString() Then
            Me.Text = String.Format("Expense History For {0}", ExpenseDetails.ExpenseDescription)
        Else
            Me.Text = String.Format("Income History For {0}", ExpenseDetails.ExpenseDescription)
        End If

        ' Load all paid expense history for the selected expense into the window.
        For Each Record In HistoryRecords.OrderBy(Function(e) e.ExpenseActualDate).ToList()
            Dim ExpenseItem As New ListViewItem
            ExpenseItem.Text = Record.ExpenseEstimatedDate.ToShortDateString()
            ExpenseItem.SubItems.Add(Record.ExpenseActualDate.Value.ToShortDateString())
            ExpenseItem.SubItems.Add(ExpenseDetails.ExpenseDescription)
            ExpenseItem.SubItems.Add(Record.ExpenseMemo)
            ExpenseItem.SubItems.Add(FormatCurrency(Record.ExpenseEstimatedAmount))
            ExpenseItem.SubItems.Add(FormatCurrency(Record.ExpensePaidAmount))
            ExpenseItem.Tag = Record.ExpenseHistoryId

            Me.ListViewPaidExpenses.Items.Add(ExpenseItem)
        Next

    End Sub

#End Region

End Class