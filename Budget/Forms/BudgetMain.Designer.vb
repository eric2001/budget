﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BudgetMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BudgetMain))
        Me.MainMenu = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddExpenseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditExpenseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteExpenseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowExpenseHistoryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.AddIncomeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditIncomeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteIncomeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowIncomeHistoryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.SaveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStripRecurringExpenses = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AddExpenseToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditExpenseToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteExpenseToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowExpenseHistoryToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.statusStripExpenses = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabelEstimated = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusEstimatedAmount = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabelEstimatedIncome = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabelEstimatedIncomeAmount = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabelTotalPaid = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusTotalPaid = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabelEstimatedRemaining = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusEstimatedRemaining = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabelEstimatedRemainingIncome = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabelEstimatedRemainingIncomeAmount = New System.Windows.Forms.ToolStripStatusLabel()
        Me.PanelMain = New System.Windows.Forms.Panel()
        Me.Splitter2 = New System.Windows.Forms.Splitter()
        Me.GroupBoxNotes = New System.Windows.Forms.GroupBox()
        Me.txtNotes = New System.Windows.Forms.TextBox()
        Me.PanelExpenses = New System.Windows.Forms.Panel()
        Me.PanelMonthlyExpenses = New System.Windows.Forms.Panel()
        Me.groupMonthExpenses = New System.Windows.Forms.GroupBox()
        Me.ListViewProjectedExpenses = New System.Windows.Forms.ListView()
        Me.ColumnDueDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnDescription = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnAmount = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.buttonNextMonth = New System.Windows.Forms.Button()
        Me.buttonPreviousMonth = New System.Windows.Forms.Button()
        Me.Splitter1 = New System.Windows.Forms.Splitter()
        Me.PanelRecurringConfigurations = New System.Windows.Forms.Panel()
        Me.Splitter4 = New System.Windows.Forms.Splitter()
        Me.PanelRecurringExpenses = New System.Windows.Forms.Panel()
        Me.groupRecurringExpenses = New System.Windows.Forms.GroupBox()
        Me.treeRecurringExpenses = New System.Windows.Forms.TreeView()
        Me.PanelRecurringIncome = New System.Windows.Forms.Panel()
        Me.groupRecurringIncome = New System.Windows.Forms.GroupBox()
        Me.treeRecurringIncome = New System.Windows.Forms.TreeView()
        Me.ContextMenuStripRecurringIncome = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AddIncomeToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditIncomeToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteIncomeToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowIncomeHistoryToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MainMenu.SuspendLayout()
        Me.ContextMenuStripRecurringExpenses.SuspendLayout()
        Me.statusStripExpenses.SuspendLayout()
        Me.PanelMain.SuspendLayout()
        Me.GroupBoxNotes.SuspendLayout()
        Me.PanelExpenses.SuspendLayout()
        Me.PanelMonthlyExpenses.SuspendLayout()
        Me.groupMonthExpenses.SuspendLayout()
        Me.PanelRecurringConfigurations.SuspendLayout()
        Me.PanelRecurringExpenses.SuspendLayout()
        Me.groupRecurringExpenses.SuspendLayout()
        Me.PanelRecurringIncome.SuspendLayout()
        Me.groupRecurringIncome.SuspendLayout()
        Me.ContextMenuStripRecurringIncome.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainMenu
        '
        Me.MainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.OptionsToolStripMenuItem})
        Me.MainMenu.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu.Name = "MainMenu"
        Me.MainMenu.Size = New System.Drawing.Size(984, 24)
        Me.MainMenu.TabIndex = 0
        Me.MainMenu.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddExpenseToolStripMenuItem, Me.EditExpenseToolStripMenuItem, Me.DeleteExpenseToolStripMenuItem, Me.ShowExpenseHistoryToolStripMenuItem, Me.ToolStripMenuItem1, Me.AddIncomeToolStripMenuItem, Me.EditIncomeToolStripMenuItem, Me.DeleteIncomeToolStripMenuItem, Me.ShowIncomeHistoryToolStripMenuItem, Me.ToolStripMenuItem3, Me.SaveToolStripMenuItem, Me.ToolStripMenuItem2, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'AddExpenseToolStripMenuItem
        '
        Me.AddExpenseToolStripMenuItem.Name = "AddExpenseToolStripMenuItem"
        Me.AddExpenseToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.AddExpenseToolStripMenuItem.Text = "Add Expense"
        '
        'EditExpenseToolStripMenuItem
        '
        Me.EditExpenseToolStripMenuItem.Name = "EditExpenseToolStripMenuItem"
        Me.EditExpenseToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.EditExpenseToolStripMenuItem.Text = "Edit Expense"
        '
        'DeleteExpenseToolStripMenuItem
        '
        Me.DeleteExpenseToolStripMenuItem.Name = "DeleteExpenseToolStripMenuItem"
        Me.DeleteExpenseToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.DeleteExpenseToolStripMenuItem.Text = "Delete Expense"
        '
        'ShowExpenseHistoryToolStripMenuItem
        '
        Me.ShowExpenseHistoryToolStripMenuItem.Name = "ShowExpenseHistoryToolStripMenuItem"
        Me.ShowExpenseHistoryToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.ShowExpenseHistoryToolStripMenuItem.Text = "Show Expense History"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(186, 6)
        '
        'AddIncomeToolStripMenuItem
        '
        Me.AddIncomeToolStripMenuItem.Name = "AddIncomeToolStripMenuItem"
        Me.AddIncomeToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.AddIncomeToolStripMenuItem.Text = "Add Income"
        '
        'EditIncomeToolStripMenuItem
        '
        Me.EditIncomeToolStripMenuItem.Name = "EditIncomeToolStripMenuItem"
        Me.EditIncomeToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.EditIncomeToolStripMenuItem.Text = "Edit Income"
        '
        'DeleteIncomeToolStripMenuItem
        '
        Me.DeleteIncomeToolStripMenuItem.Name = "DeleteIncomeToolStripMenuItem"
        Me.DeleteIncomeToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.DeleteIncomeToolStripMenuItem.Text = "Delete Income"
        '
        'ShowIncomeHistoryToolStripMenuItem
        '
        Me.ShowIncomeHistoryToolStripMenuItem.Name = "ShowIncomeHistoryToolStripMenuItem"
        Me.ShowIncomeHistoryToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.ShowIncomeHistoryToolStripMenuItem.Text = "Show Income History"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(186, 6)
        '
        'SaveToolStripMenuItem
        '
        Me.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem"
        Me.SaveToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.SaveToolStripMenuItem.Text = "Save"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(186, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'OptionsToolStripMenuItem
        '
        Me.OptionsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem})
        Me.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem"
        Me.OptionsToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.OptionsToolStripMenuItem.Text = "Options"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'ContextMenuStripRecurringExpenses
        '
        Me.ContextMenuStripRecurringExpenses.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddExpenseToolStripMenuItem1, Me.EditExpenseToolStripMenuItem1, Me.DeleteExpenseToolStripMenuItem1, Me.ShowExpenseHistoryToolStripMenuItem1})
        Me.ContextMenuStripRecurringExpenses.Name = "ContextMenuStripRecurringExpenses"
        Me.ContextMenuStripRecurringExpenses.Size = New System.Drawing.Size(190, 92)
        '
        'AddExpenseToolStripMenuItem1
        '
        Me.AddExpenseToolStripMenuItem1.Name = "AddExpenseToolStripMenuItem1"
        Me.AddExpenseToolStripMenuItem1.Size = New System.Drawing.Size(189, 22)
        Me.AddExpenseToolStripMenuItem1.Text = "Add Expense"
        '
        'EditExpenseToolStripMenuItem1
        '
        Me.EditExpenseToolStripMenuItem1.Name = "EditExpenseToolStripMenuItem1"
        Me.EditExpenseToolStripMenuItem1.Size = New System.Drawing.Size(189, 22)
        Me.EditExpenseToolStripMenuItem1.Text = "Edit Expense"
        '
        'DeleteExpenseToolStripMenuItem1
        '
        Me.DeleteExpenseToolStripMenuItem1.Name = "DeleteExpenseToolStripMenuItem1"
        Me.DeleteExpenseToolStripMenuItem1.Size = New System.Drawing.Size(189, 22)
        Me.DeleteExpenseToolStripMenuItem1.Text = "Delete Expense"
        '
        'ShowExpenseHistoryToolStripMenuItem1
        '
        Me.ShowExpenseHistoryToolStripMenuItem1.Name = "ShowExpenseHistoryToolStripMenuItem1"
        Me.ShowExpenseHistoryToolStripMenuItem1.Size = New System.Drawing.Size(189, 22)
        Me.ShowExpenseHistoryToolStripMenuItem1.Text = "Show Expense History"
        '
        'statusStripExpenses
        '
        Me.statusStripExpenses.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabelEstimated, Me.ToolStripStatusEstimatedAmount, Me.ToolStripStatusLabelEstimatedIncome, Me.ToolStripStatusLabelEstimatedIncomeAmount, Me.ToolStripStatusLabelTotalPaid, Me.ToolStripStatusTotalPaid, Me.ToolStripStatusLabelEstimatedRemaining, Me.ToolStripStatusEstimatedRemaining, Me.ToolStripStatusLabelEstimatedRemainingIncome, Me.ToolStripStatusLabelEstimatedRemainingIncomeAmount})
        Me.statusStripExpenses.Location = New System.Drawing.Point(0, 739)
        Me.statusStripExpenses.Name = "statusStripExpenses"
        Me.statusStripExpenses.Size = New System.Drawing.Size(984, 22)
        Me.statusStripExpenses.TabIndex = 7
        Me.statusStripExpenses.Text = "StatusStrip1"
        '
        'ToolStripStatusLabelEstimated
        '
        Me.ToolStripStatusLabelEstimated.Name = "ToolStripStatusLabelEstimated"
        Me.ToolStripStatusLabelEstimated.Size = New System.Drawing.Size(112, 17)
        Me.ToolStripStatusLabelEstimated.Text = "Estimated Expenses:"
        '
        'ToolStripStatusEstimatedAmount
        '
        Me.ToolStripStatusEstimatedAmount.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolStripStatusEstimatedAmount.Name = "ToolStripStatusEstimatedAmount"
        Me.ToolStripStatusEstimatedAmount.Size = New System.Drawing.Size(40, 17)
        Me.ToolStripStatusEstimatedAmount.Text = "$00.00"
        '
        'ToolStripStatusLabelEstimatedIncome
        '
        Me.ToolStripStatusLabelEstimatedIncome.Name = "ToolStripStatusLabelEstimatedIncome"
        Me.ToolStripStatusLabelEstimatedIncome.Size = New System.Drawing.Size(105, 17)
        Me.ToolStripStatusLabelEstimatedIncome.Text = "Estimated Income:"
        '
        'ToolStripStatusLabelEstimatedIncomeAmount
        '
        Me.ToolStripStatusLabelEstimatedIncomeAmount.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolStripStatusLabelEstimatedIncomeAmount.Name = "ToolStripStatusLabelEstimatedIncomeAmount"
        Me.ToolStripStatusLabelEstimatedIncomeAmount.Size = New System.Drawing.Size(40, 17)
        Me.ToolStripStatusLabelEstimatedIncomeAmount.Text = "$00.00"
        '
        'ToolStripStatusLabelTotalPaid
        '
        Me.ToolStripStatusLabelTotalPaid.Name = "ToolStripStatusLabelTotalPaid"
        Me.ToolStripStatusLabelTotalPaid.Size = New System.Drawing.Size(62, 17)
        Me.ToolStripStatusLabelTotalPaid.Text = "Total Paid:"
        '
        'ToolStripStatusTotalPaid
        '
        Me.ToolStripStatusTotalPaid.ForeColor = System.Drawing.Color.Green
        Me.ToolStripStatusTotalPaid.Name = "ToolStripStatusTotalPaid"
        Me.ToolStripStatusTotalPaid.Size = New System.Drawing.Size(40, 17)
        Me.ToolStripStatusTotalPaid.Text = "$00.00"
        '
        'ToolStripStatusLabelEstimatedRemaining
        '
        Me.ToolStripStatusLabelEstimatedRemaining.Name = "ToolStripStatusLabelEstimatedRemaining"
        Me.ToolStripStatusLabelEstimatedRemaining.Size = New System.Drawing.Size(103, 17)
        Me.ToolStripStatusLabelEstimatedRemaining.Text = "Estimated Unpaid:"
        '
        'ToolStripStatusEstimatedRemaining
        '
        Me.ToolStripStatusEstimatedRemaining.ForeColor = System.Drawing.Color.Red
        Me.ToolStripStatusEstimatedRemaining.Name = "ToolStripStatusEstimatedRemaining"
        Me.ToolStripStatusEstimatedRemaining.Size = New System.Drawing.Size(40, 17)
        Me.ToolStripStatusEstimatedRemaining.Text = "$00.00"
        '
        'ToolStripStatusLabelEstimatedRemainingIncome
        '
        Me.ToolStripStatusLabelEstimatedRemainingIncome.Name = "ToolStripStatusLabelEstimatedRemainingIncome"
        Me.ToolStripStatusLabelEstimatedRemainingIncome.Size = New System.Drawing.Size(148, 17)
        Me.ToolStripStatusLabelEstimatedRemainingIncome.Text = "Estimated Unused Income:"
        '
        'ToolStripStatusLabelEstimatedRemainingIncomeAmount
        '
        Me.ToolStripStatusLabelEstimatedRemainingIncomeAmount.Name = "ToolStripStatusLabelEstimatedRemainingIncomeAmount"
        Me.ToolStripStatusLabelEstimatedRemainingIncomeAmount.Size = New System.Drawing.Size(40, 17)
        Me.ToolStripStatusLabelEstimatedRemainingIncomeAmount.Text = "$00.00"
        '
        'PanelMain
        '
        Me.PanelMain.Controls.Add(Me.Splitter2)
        Me.PanelMain.Controls.Add(Me.GroupBoxNotes)
        Me.PanelMain.Controls.Add(Me.PanelExpenses)
        Me.PanelMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelMain.Location = New System.Drawing.Point(0, 24)
        Me.PanelMain.Name = "PanelMain"
        Me.PanelMain.Size = New System.Drawing.Size(984, 715)
        Me.PanelMain.TabIndex = 8
        '
        'Splitter2
        '
        Me.Splitter2.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Splitter2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Splitter2.Location = New System.Drawing.Point(750, 0)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(3, 715)
        Me.Splitter2.TabIndex = 9
        Me.Splitter2.TabStop = False
        '
        'GroupBoxNotes
        '
        Me.GroupBoxNotes.Controls.Add(Me.txtNotes)
        Me.GroupBoxNotes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBoxNotes.Location = New System.Drawing.Point(750, 0)
        Me.GroupBoxNotes.Name = "GroupBoxNotes"
        Me.GroupBoxNotes.Size = New System.Drawing.Size(234, 715)
        Me.GroupBoxNotes.TabIndex = 8
        Me.GroupBoxNotes.TabStop = False
        Me.GroupBoxNotes.Text = "Notes"
        '
        'txtNotes
        '
        Me.txtNotes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtNotes.Location = New System.Drawing.Point(3, 16)
        Me.txtNotes.Multiline = True
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtNotes.Size = New System.Drawing.Size(228, 696)
        Me.txtNotes.TabIndex = 0
        '
        'PanelExpenses
        '
        Me.PanelExpenses.Controls.Add(Me.PanelMonthlyExpenses)
        Me.PanelExpenses.Controls.Add(Me.Splitter1)
        Me.PanelExpenses.Controls.Add(Me.PanelRecurringConfigurations)
        Me.PanelExpenses.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelExpenses.Location = New System.Drawing.Point(0, 0)
        Me.PanelExpenses.Name = "PanelExpenses"
        Me.PanelExpenses.Size = New System.Drawing.Size(750, 715)
        Me.PanelExpenses.TabIndex = 7
        '
        'PanelMonthlyExpenses
        '
        Me.PanelMonthlyExpenses.Controls.Add(Me.groupMonthExpenses)
        Me.PanelMonthlyExpenses.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelMonthlyExpenses.Location = New System.Drawing.Point(203, 0)
        Me.PanelMonthlyExpenses.Name = "PanelMonthlyExpenses"
        Me.PanelMonthlyExpenses.Size = New System.Drawing.Size(547, 715)
        Me.PanelMonthlyExpenses.TabIndex = 14
        '
        'groupMonthExpenses
        '
        Me.groupMonthExpenses.Controls.Add(Me.ListViewProjectedExpenses)
        Me.groupMonthExpenses.Controls.Add(Me.buttonNextMonth)
        Me.groupMonthExpenses.Controls.Add(Me.buttonPreviousMonth)
        Me.groupMonthExpenses.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupMonthExpenses.Location = New System.Drawing.Point(0, 0)
        Me.groupMonthExpenses.Name = "groupMonthExpenses"
        Me.groupMonthExpenses.Size = New System.Drawing.Size(547, 715)
        Me.groupMonthExpenses.TabIndex = 12
        Me.groupMonthExpenses.TabStop = False
        Me.groupMonthExpenses.Text = "Expenses For - %MONTH%"
        '
        'ListViewProjectedExpenses
        '
        Me.ListViewProjectedExpenses.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListViewProjectedExpenses.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnDueDate, Me.ColumnDescription, Me.ColumnAmount})
        Me.ListViewProjectedExpenses.FullRowSelect = True
        Me.ListViewProjectedExpenses.HideSelection = False
        Me.ListViewProjectedExpenses.Location = New System.Drawing.Point(3, 50)
        Me.ListViewProjectedExpenses.MultiSelect = False
        Me.ListViewProjectedExpenses.Name = "ListViewProjectedExpenses"
        Me.ListViewProjectedExpenses.Size = New System.Drawing.Size(541, 662)
        Me.ListViewProjectedExpenses.TabIndex = 5
        Me.ListViewProjectedExpenses.UseCompatibleStateImageBehavior = False
        Me.ListViewProjectedExpenses.View = System.Windows.Forms.View.Details
        '
        'ColumnDueDate
        '
        Me.ColumnDueDate.Text = "Date"
        Me.ColumnDueDate.Width = 120
        '
        'ColumnDescription
        '
        Me.ColumnDescription.Text = "Description"
        Me.ColumnDescription.Width = 315
        '
        'ColumnAmount
        '
        Me.ColumnAmount.Text = "Amount"
        Me.ColumnAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnAmount.Width = 100
        '
        'buttonNextMonth
        '
        Me.buttonNextMonth.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonNextMonth.Location = New System.Drawing.Point(431, 20)
        Me.buttonNextMonth.Name = "buttonNextMonth"
        Me.buttonNextMonth.Size = New System.Drawing.Size(104, 23)
        Me.buttonNextMonth.TabIndex = 1
        Me.buttonNextMonth.Text = "Next Month >>"
        Me.buttonNextMonth.UseVisualStyleBackColor = True
        '
        'buttonPreviousMonth
        '
        Me.buttonPreviousMonth.Location = New System.Drawing.Point(7, 20)
        Me.buttonPreviousMonth.Name = "buttonPreviousMonth"
        Me.buttonPreviousMonth.Size = New System.Drawing.Size(110, 23)
        Me.buttonPreviousMonth.TabIndex = 0
        Me.buttonPreviousMonth.Text = "<< Previous Month"
        Me.buttonPreviousMonth.UseVisualStyleBackColor = True
        '
        'Splitter1
        '
        Me.Splitter1.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Splitter1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Splitter1.Location = New System.Drawing.Point(200, 0)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(3, 715)
        Me.Splitter1.TabIndex = 13
        Me.Splitter1.TabStop = False
        '
        'PanelRecurringConfigurations
        '
        Me.PanelRecurringConfigurations.Controls.Add(Me.Splitter4)
        Me.PanelRecurringConfigurations.Controls.Add(Me.PanelRecurringExpenses)
        Me.PanelRecurringConfigurations.Controls.Add(Me.PanelRecurringIncome)
        Me.PanelRecurringConfigurations.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelRecurringConfigurations.Location = New System.Drawing.Point(0, 0)
        Me.PanelRecurringConfigurations.Name = "PanelRecurringConfigurations"
        Me.PanelRecurringConfigurations.Size = New System.Drawing.Size(200, 715)
        Me.PanelRecurringConfigurations.TabIndex = 12
        '
        'Splitter4
        '
        Me.Splitter4.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Splitter4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Splitter4.Cursor = System.Windows.Forms.Cursors.HSplit
        Me.Splitter4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Splitter4.Location = New System.Drawing.Point(0, 512)
        Me.Splitter4.Name = "Splitter4"
        Me.Splitter4.Size = New System.Drawing.Size(200, 3)
        Me.Splitter4.TabIndex = 13
        Me.Splitter4.TabStop = False
        '
        'PanelRecurringExpenses
        '
        Me.PanelRecurringExpenses.Controls.Add(Me.groupRecurringExpenses)
        Me.PanelRecurringExpenses.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelRecurringExpenses.Location = New System.Drawing.Point(0, 0)
        Me.PanelRecurringExpenses.Name = "PanelRecurringExpenses"
        Me.PanelRecurringExpenses.Size = New System.Drawing.Size(200, 515)
        Me.PanelRecurringExpenses.TabIndex = 12
        '
        'groupRecurringExpenses
        '
        Me.groupRecurringExpenses.Controls.Add(Me.treeRecurringExpenses)
        Me.groupRecurringExpenses.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupRecurringExpenses.Location = New System.Drawing.Point(0, 0)
        Me.groupRecurringExpenses.Name = "groupRecurringExpenses"
        Me.groupRecurringExpenses.Size = New System.Drawing.Size(200, 515)
        Me.groupRecurringExpenses.TabIndex = 11
        Me.groupRecurringExpenses.TabStop = False
        Me.groupRecurringExpenses.Text = "Recurring Expenses"
        '
        'treeRecurringExpenses
        '
        Me.treeRecurringExpenses.ContextMenuStrip = Me.ContextMenuStripRecurringExpenses
        Me.treeRecurringExpenses.Dock = System.Windows.Forms.DockStyle.Fill
        Me.treeRecurringExpenses.HideSelection = False
        Me.treeRecurringExpenses.Location = New System.Drawing.Point(3, 16)
        Me.treeRecurringExpenses.Name = "treeRecurringExpenses"
        Me.treeRecurringExpenses.Size = New System.Drawing.Size(194, 496)
        Me.treeRecurringExpenses.TabIndex = 0
        '
        'PanelRecurringIncome
        '
        Me.PanelRecurringIncome.Controls.Add(Me.groupRecurringIncome)
        Me.PanelRecurringIncome.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelRecurringIncome.Location = New System.Drawing.Point(0, 515)
        Me.PanelRecurringIncome.Name = "PanelRecurringIncome"
        Me.PanelRecurringIncome.Size = New System.Drawing.Size(200, 200)
        Me.PanelRecurringIncome.TabIndex = 11
        '
        'groupRecurringIncome
        '
        Me.groupRecurringIncome.Controls.Add(Me.treeRecurringIncome)
        Me.groupRecurringIncome.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupRecurringIncome.Location = New System.Drawing.Point(0, 0)
        Me.groupRecurringIncome.Name = "groupRecurringIncome"
        Me.groupRecurringIncome.Size = New System.Drawing.Size(200, 200)
        Me.groupRecurringIncome.TabIndex = 0
        Me.groupRecurringIncome.TabStop = False
        Me.groupRecurringIncome.Text = "Recurring Income"
        '
        'treeRecurringIncome
        '
        Me.treeRecurringIncome.ContextMenuStrip = Me.ContextMenuStripRecurringIncome
        Me.treeRecurringIncome.Dock = System.Windows.Forms.DockStyle.Fill
        Me.treeRecurringIncome.HideSelection = False
        Me.treeRecurringIncome.Location = New System.Drawing.Point(3, 16)
        Me.treeRecurringIncome.Name = "treeRecurringIncome"
        Me.treeRecurringIncome.Size = New System.Drawing.Size(194, 181)
        Me.treeRecurringIncome.TabIndex = 1
        '
        'ContextMenuStripRecurringIncome
        '
        Me.ContextMenuStripRecurringIncome.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddIncomeToolStripMenuItem1, Me.EditIncomeToolStripMenuItem1, Me.DeleteIncomeToolStripMenuItem1, Me.ShowIncomeHistoryToolStripMenuItem1})
        Me.ContextMenuStripRecurringIncome.Name = "ContextMenuStripRecurringIncome"
        Me.ContextMenuStripRecurringIncome.Size = New System.Drawing.Size(188, 92)
        '
        'AddIncomeToolStripMenuItem1
        '
        Me.AddIncomeToolStripMenuItem1.Name = "AddIncomeToolStripMenuItem1"
        Me.AddIncomeToolStripMenuItem1.Size = New System.Drawing.Size(187, 22)
        Me.AddIncomeToolStripMenuItem1.Text = "Add Income"
        '
        'EditIncomeToolStripMenuItem1
        '
        Me.EditIncomeToolStripMenuItem1.Name = "EditIncomeToolStripMenuItem1"
        Me.EditIncomeToolStripMenuItem1.Size = New System.Drawing.Size(187, 22)
        Me.EditIncomeToolStripMenuItem1.Text = "Edit Income"
        '
        'DeleteIncomeToolStripMenuItem1
        '
        Me.DeleteIncomeToolStripMenuItem1.Name = "DeleteIncomeToolStripMenuItem1"
        Me.DeleteIncomeToolStripMenuItem1.Size = New System.Drawing.Size(187, 22)
        Me.DeleteIncomeToolStripMenuItem1.Text = "Delete Income"
        '
        'ShowIncomeHistoryToolStripMenuItem1
        '
        Me.ShowIncomeHistoryToolStripMenuItem1.Name = "ShowIncomeHistoryToolStripMenuItem1"
        Me.ShowIncomeHistoryToolStripMenuItem1.Size = New System.Drawing.Size(187, 22)
        Me.ShowIncomeHistoryToolStripMenuItem1.Text = "Show Income History"
        '
        'BudgetMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 761)
        Me.Controls.Add(Me.PanelMain)
        Me.Controls.Add(Me.statusStripExpenses)
        Me.Controls.Add(Me.MainMenu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MainMenu
        Me.Name = "BudgetMain"
        Me.Text = "Budget"
        Me.MainMenu.ResumeLayout(False)
        Me.MainMenu.PerformLayout()
        Me.ContextMenuStripRecurringExpenses.ResumeLayout(False)
        Me.statusStripExpenses.ResumeLayout(False)
        Me.statusStripExpenses.PerformLayout()
        Me.PanelMain.ResumeLayout(False)
        Me.GroupBoxNotes.ResumeLayout(False)
        Me.GroupBoxNotes.PerformLayout()
        Me.PanelExpenses.ResumeLayout(False)
        Me.PanelMonthlyExpenses.ResumeLayout(False)
        Me.groupMonthExpenses.ResumeLayout(False)
        Me.PanelRecurringConfigurations.ResumeLayout(False)
        Me.PanelRecurringExpenses.ResumeLayout(False)
        Me.groupRecurringExpenses.ResumeLayout(False)
        Me.PanelRecurringIncome.ResumeLayout(False)
        Me.groupRecurringIncome.ResumeLayout(False)
        Me.ContextMenuStripRecurringIncome.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MainMenu As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AddExpenseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EditExpenseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ShowExpenseHistoryToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SaveToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripSeparator
    Friend WithEvents DeleteExpenseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ContextMenuStripRecurringExpenses As ContextMenuStrip
    Friend WithEvents AddExpenseToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents EditExpenseToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents DeleteExpenseToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ShowExpenseHistoryToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents statusStripExpenses As StatusStrip
    Friend WithEvents ToolStripStatusLabelEstimated As ToolStripStatusLabel
    Friend WithEvents ToolStripStatusEstimatedAmount As ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabelTotalPaid As ToolStripStatusLabel
    Friend WithEvents ToolStripStatusTotalPaid As ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabelEstimatedRemaining As ToolStripStatusLabel
    Friend WithEvents ToolStripStatusEstimatedRemaining As ToolStripStatusLabel
    Friend WithEvents PanelMain As Panel
    Friend WithEvents Splitter2 As Splitter
    Friend WithEvents GroupBoxNotes As GroupBox
    Friend WithEvents txtNotes As TextBox
    Friend WithEvents PanelExpenses As Panel
    Friend WithEvents ToolStripStatusLabelEstimatedIncome As ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabelEstimatedIncomeAmount As ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabelEstimatedRemainingIncome As ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabelEstimatedRemainingIncomeAmount As ToolStripStatusLabel
    Friend WithEvents Splitter1 As Splitter
    Friend WithEvents PanelRecurringConfigurations As Panel
    Friend WithEvents PanelMonthlyExpenses As Panel
    Friend WithEvents groupMonthExpenses As GroupBox
    Friend WithEvents ListViewProjectedExpenses As ListView
    Friend WithEvents ColumnDueDate As ColumnHeader
    Friend WithEvents ColumnDescription As ColumnHeader
    Friend WithEvents ColumnAmount As ColumnHeader
    Friend WithEvents buttonNextMonth As Button
    Friend WithEvents buttonPreviousMonth As Button
    Friend WithEvents AddIncomeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EditIncomeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DeleteIncomeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ShowIncomeHistoryToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As ToolStripSeparator
    Friend WithEvents Splitter4 As Splitter
    Friend WithEvents PanelRecurringExpenses As Panel
    Friend WithEvents groupRecurringExpenses As GroupBox
    Friend WithEvents treeRecurringExpenses As TreeView
    Friend WithEvents PanelRecurringIncome As Panel
    Friend WithEvents groupRecurringIncome As GroupBox
    Friend WithEvents treeRecurringIncome As TreeView
    Friend WithEvents ContextMenuStripRecurringIncome As ContextMenuStrip
    Friend WithEvents AddIncomeToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents EditIncomeToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents DeleteIncomeToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ShowIncomeHistoryToolStripMenuItem1 As ToolStripMenuItem
End Class
