# Budget
![Budget](./Budget1.1.0.png) 

## Description
The purpose of this project is to help track and manage a monthly budget.

**License:** [GPL v.3](http://www.gnu.org/copyleft/gpl.html)

## Installation
 - Extract the contents of the binary .zip file to a folder and run the Budget.exe file.  

## Upgrading
 - Same as installation instructions, but backup your Data/Budget.xml file before updating the files, then restore the backup copy over the new one in the release.  
 - This file is were all of your saved data goes to.

## Instructions
Adding new income / expenses
 - To Start, go to File -> Add Expense or Add Income. 
 - From here you can configure what amount this income / expense is for, what date this should start on, how often this should repeat (weekly, every other week, monthly, yearly, etc) and a description.
 - You can also optionally set an end date, which is the last date this income / expense should appear on.
 - You can also configure if this income / expense should repeat based on the initial due date or the date it was last paid on.

Editing Income / Expenses
 - To edit an income / expense, either select it from the list of the left and double click, or select it and click File -> Edit Expense or Edit Income.  You can also right click and select Edit.

Show Income / Expense History
 - This will show you every time that the selected income / expense has been paid, and for what amount.
 - Select an income or expense from the list at the left, and then click on File -> Show Expense History or Show Income History to view this information.  You can also right click on the item.

Marking Expense as Paid / Marking Income as Received
 - Once you have income and expenses configured in the list on the left, you'll start seeing them calculate and populate in the gird on the right.
 - An income / expense will show up as Green if it has been paid, Red if it is due on or before today, or black if it is in the future.
 - To mark an income / expense as paid, double click on it and enter in the date it was paid on and the amount.  You can also supply any optional notes in the Memo field.
 - The grid will then refresh if needed and update future dates if this income / expense is configured to calculate from date it was last paid on.
 - Once an income / expense is marked as paid, the estimated amount for the month will re-calculate based on the amount that was actually paid for the expense and the total paid and remaining amount fields will update.
 - NOTE:  Expenses are calculated forward from their last paid date.  
 -- If you set an expense to start on January 1st, then skip to June and mark June 1st as paid, the assumption is that everything before was skipped / covered by the June 1st payment and will thus be removed.
 - You can double click on a paid expense to update it's details later if needed.

Past Due Income / Expenses
 - Any income or expenses that are calculated as being due on or before today will show up as red.
 - They will also show up in the Past Due Expenses group or Past Due Income group at the top of the grid.
 - These expenses are displayed at the top of the grid for reference only and are not taken into account when calculating the totals at the bottom, unless they are also in the Monthly Expenses group.

Numbers at the Bottom
 - The following values are calculated and displayed at the bottom of the window:  Estimated Expenses, Estimated Income, Total Paid, Estimated Unpaid, Estimated Unused Income.
 - These numbers are calculated from the items in the Monthly Expenses and Monthly Income groups.  They do not include items in the Past Due groups, unless those items are also in the monthly group.
 - Estimated Expenses is the total of the Amount column in the Monthly Expenses group.  This value will update from estimated amounts to paid amounts as expenses are marked as paid.
 - Estimated Income is the total of the Amount column for the Monthly Income group.  This value will update from estimated to received amounts as income is marked as received.
 - Total Paid is the total of all items in the Monthly Expenses group that have been marked as paid.
 - Estimated Unpaid is the total of all items in the Monthly Expenses group that have not been marked as paid.
 - Estimated Unused Income is the amount of estimated income that will not be used by the estimated expenses for the month.  This will display as green if the number is positive (more income then expenses) or red if the number is negative (more expenses then income).
 
## History
**Version 1.2.0:**
> - Added support to double click to edit recurring income configurations.
> - Updated Expense History grid to select full row instead of first cell in selected row.
> - Optimized calculation logic to only re-calculate the parts that have been changed to improve performance.
> - Misc code optimizations and improvements.
> - Released 29 July 2018
>
> Download: [Version 1.2.0 Binary](/uploads/c2ae9c32086ef055935320fbf5f08c03/Budget1.2.0Binary.zip) | [Version 1.2.0 Source](/uploads/e381183a9aeecab3f113e49b1e8bf45d/Budget1.2.0Source.zip)

**Version 1.1.0:**
> - Added ability to delete recurring expenses.
> - Added Right Click menus for recurring expenses.
> - Added a new Memo field to payment history.
> - Added a "Notes" section to the right of the screen.
> - Added additional columns to the payment history screen.
> - Added support for Income tracking.
> - Misc bug fixes and code restructuring.
> - Released 07 July 2018
>
> Download: [Version 1.1.0 Binary](/uploads/24580fff74a89f0e1fd555639fe295a5/Budget1.1.0Binary.zip) | [Version 1.1.0 Source](/uploads/3d905269e0d1d324a2ec4f4d26844489/Budget1.1.0Source.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 30 June 2018
>
> Download: [Version 1.0.0 Binary](/uploads/f4bd7c4fbbd626dcf2a983999d558618/Budget1.0.0Binary.zip) | [Version 1.0.0 Source](/uploads/6c48f09d60bc2c6b48928bc9968ee73e/Budget1.0.0Source.zip)
